#  G-Tix - Cinema ticket booking website
This thesis project involves the development of G-Tix, a web-based application for booking movie tickets. The focus of this study is on the implementation of the Jaccard Similarity algorithm and machine learning using the Long Short Term Memory (LSTM) method. G-Tix is a full-stack web application built using React.js for the front end and Express.js for the back end.

### Features
- 𝗠𝗶𝗱𝘁𝗿𝗮𝗻𝘀 𝗣𝗮𝘆𝗺𝗲𝗻𝘁 𝗚𝗮𝘁𝗲𝘄𝗮𝘆: Integration of a secure payment gateway to facilitate online transactions.
- 𝗢𝗔𝘂𝘁𝗵: Login with Google account.
- 𝗧𝗲𝗻𝘀𝗼𝗿𝗙𝗹𝗼𝘄 𝗳𝗼𝗿 𝗠𝗮𝗰𝗵𝗶𝗻𝗲 𝗟𝗲𝗮𝗿𝗻𝗶𝗻𝗴 𝘄𝗶𝘁𝗵 𝗟𝗦𝗧𝗠: Implementation of TensorFlow for LSTM to schedule movie screenings.
- 𝗝𝗮𝗰𝗰𝗮𝗿𝗱 𝗦𝗶𝗺𝗶𝗹𝗮𝗿𝗶𝘁𝘆 𝗔𝗹𝗴𝗼𝗿𝗶𝘁𝗵𝗺: Use of the Jaccard Similarity algorithm for the movie recommendation system.
- 𝗢𝗻𝗲-𝗧𝗶𝗺𝗲 𝗣𝗮𝘀𝘀𝘄𝗼𝗿𝗱 (𝗢𝗧𝗣): Implementation of OTP for register account and password reset functionality.
- 𝗩𝗮𝗿𝗶𝗼𝘂𝘀 𝗩𝗮𝗹𝗶𝗱𝗮𝘁𝗶𝗼𝗻𝘀: Numerous validation mechanisms to ensure data integrity and user experience.

## Technology Used
[![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white)](https://nodejs.org)
[![Express.js](https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB)](https://create-react-app.dev/)
[![Tailwind Css](https://img.shields.io/badge/Tailwind_CSS-38B2AC?style=for-the-badge&logo=tailwind-css&logoColor=white)](https://tailwindcss.com/docs/guides/create-react-app)
---

## 🔗 Connect with Me
[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/xhudan)
[![instagram](https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white)](https://instagram.com/mhudangrh)
[![Facebook](https://img.shields.io/badge/Facebook-1877F2?style=for-the-badge&logo=facebook&logoColor=white)](https://facebook.com/mhudangrh)
