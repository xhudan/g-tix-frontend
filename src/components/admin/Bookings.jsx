import { useState } from "react";
import { useGetAllHistoryQuery } from "../../config/react-query/book";
import { FaFilter } from "react-icons/fa";
import DatePicker from "react-multi-date-picker";
import "react-multi-date-picker/styles/layouts/mobile.css";
import "react-multi-date-picker/styles/backgrounds/bg-dark.css";
import useSlider from "../slider/Slider";

const Bookings = () => {
    const [currentPage, setCurrentPage] = useState(1);

    const { slider } = useSlider();

    const { data } = useGetAllHistoryQuery();
    const bookingData = data?.data.data || [];

    const formatDate = (value) => {
        const date = new Date(value);
        const monthNames = [
            "January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        const filterDate = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
        const format = `${monthNames[date.getMonth()]} ${filterDate}, ${date.getFullYear()}`;
        return format;
    };

    const formattedTimeOptions = (time) => {
        const [hours, minutes] = time.split(':');
        return `${hours}:${minutes}`;
    };

    const itemsPerPage = 25;

    const [selectedDates, setSelectedDates] = useState([]);

    const handleDateChange = (dates) => {
        setSelectedDates(dates);
        setCurrentPage(1); // Reset to the first page on filter change
    };
    
    const normalizeDate = (date) => {
        const normalized = new Date(date);
        normalized.setHours(0, 0, 0, 0);
        return normalized;
    };

    const getFilteredBookings = () => {
        if (selectedDates.length === 2) {
            const [startDate, endDate] = selectedDates.map(normalizeDate);
            return bookingData.filter((booking) => {
                const bookingDate = normalizeDate(new Date(booking.createdAt));
                return bookingDate >= startDate && bookingDate <= endDate;
            });
        } else if (selectedDates.length === 1) {
            const selectedDate = normalizeDate(selectedDates[0]);
            return bookingData.filter((booking) => {
                const bookingDate = normalizeDate(new Date(booking.createdAt));
                return bookingDate.getTime() === selectedDate.getTime();
            });
        }
        return bookingData;
    };

    const filteredBooking = getFilteredBookings();

    const sortedBooking = [...filteredBooking].sort((a, b) =>
        (b.createdAt || "").localeCompare(a.createdAt || "")
    );

    const totalPages = Math.ceil(sortedBooking.length / itemsPerPage);
    const paginatedBooking = sortedBooking.slice(
        (currentPage - 1) * itemsPerPage,
        currentPage * itemsPerPage
    );

    const handlePageChange = (page) => {
        setCurrentPage(page);
    };

    const renderTable = (bookings) => (
        <table className="w-full border-collapse border border-spacing-y-1 table-auto">
            <thead>
                <tr className="text-center bg-gray-500">
                    <th className="border py-1 px-2">Status</th>
                    <th className="border py-1 px-2">Booking Code</th>
                    <th className="border py-1 px-2">Movie</th>
                    <th className="border py-1 px-2">Location</th>
                    <th className="border py-1 px-2">Date</th>
                    <th className="border py-1 px-2">Showtime</th>
                    <th className="border py-1 px-2">Quantity</th>
                    <th className="border py-1 px-2">Seat Number</th>
                    <th className="border py-1 px-2">Total Price (IDR)</th>
                </tr>
            </thead>
            <tbody>
                {bookings.map((booking) => (
                    <tr key={booking.id}>
                        <td className={`border py-1 px-2 ${booking.payment_status === "Issued" ? "text-blue-500" : booking.payment_status === "Cancelled" ? "text-red-500" : ""}`}>{booking.payment_status}</td>
                        <td className="border py-1 px-2">{booking?.booking_code}</td>
                        <td className="border py-1 px-2">{booking?.movie?.title}</td>
                        <td className="border py-1 px-2">{booking?.cinema?.location}</td>
                        <td className="border py-1 px-2">{formatDate(booking?.seats[0]?.datepick)}</td>
                        <td className="border py-1 px-2">{formattedTimeOptions(booking?.seats[0]?.showtime)}</td>
                        <td className="border py-1 px-2">{booking?.total_booking}</td>
                        <td className="border py-1 px-2">{booking.seats.some(seat => seat.seat_number === null) ? "Cancelled" : booking.seats.map(seat => seat.seat_number).sort((a, b) => a - b).join(', ')}</td>
                        <td className="border py-1 px-2">{booking?.total_price?.toLocaleString("id-ID")}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    );

    return (
        <div className="text-white space-y-4">
            <div className="flex justify-end">
                <DatePicker
                    range
                    rangeHover
                    value={selectedDates.map((date) => new Date(date))}
                    onChange={handleDateChange}
                    className="bg-dark rmdp-mobile"
                    mobileButtons={[
                        {
                            label: "RESET",
                            type: "button",
                            className: "rmdp-button rmdp-action-button",
                            onClick: () => setSelectedDates([]),
                        },
                    ]}
                    render={(value, openCalendar) => (
                        <button onClick={openCalendar} className="flex relative items-center bg-blue-500 hover:bg-blue-600 rounded-lg py-1.5 pl-9 pr-3">
                            <FaFilter className="absolute pointer-events-none left-3" />
                            {value ? value : "Filter"}
                        </button>
                    )}
                />
            </div>
            <div className="overflow-auto no-select modern-scrollbar"
                ref={slider.ref}
                onMouseDown={slider.onMouseDown}
                onMouseMove={slider.onMouseMove}
                onMouseUp={slider.onMouseUp}
                onMouseLeave={slider.onMouseLeave}
            >
                {renderTable(paginatedBooking)}
            </div>
            {sortedBooking.length > paginatedBooking.length &&
                <div className="flex justify-center space-x-2">
                    {Array.from({ length: totalPages }, (_, index) => (
                        <button
                            key={index}
                            onClick={() => handlePageChange(index + 1)}
                            className={`px-3 py-1 rounded ${currentPage === index + 1 ? 'bg-blue-500' : 'bg-gray-300 text-gray-800'}`}
                        >
                            {index + 1}
                        </button>
                    ))}
                </div>
            }
        </div>
    );
}

export default Bookings;
