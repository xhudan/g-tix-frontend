import { useState } from "react";
import { useGetAllCinemasQuery, useDeleteCinemaMutation } from "../../config/react-query/cinema";
import SweetAlert from "../alert/SweetAlert";
import { FaFilter } from "react-icons/fa";
import Modal from "./cinemas/Modal";
import useSlider from "../slider/Slider";

const Cinemas = () => {
    const [filteredCategory, setFilteredCategory] = useState("All");
    const [currentPage, setCurrentPage] = useState(1);
    const [modal, setModal] = useState(false);
    const [selectedCinema, setSelectedCinema] = useState(null);

    const { slider } = useSlider();

    const { data } = useGetAllCinemasQuery();
    const cinemasData = data?.data.data || [];

    // Sort users by createdAt date
    const sortedCinemas = [...cinemasData].sort((a, b) =>
        (b.createdAt || "").localeCompare(a.createdAt || "")
    );

    const filteredCinemas = sortedCinemas.filter((cinema) => {
        if (filteredCategory === "All") {
            return true; // Show all cinemas if "All" is selected
        } else {
            // When the value from `select` is retrieved, it is always a string
            return cinema.operate === (filteredCategory === "true"); // "true" === "true" = true || "false" === "true" = false
        }
    });

    const itemsPerPage = 25;

    // Calculate total pages
    const totalPages = Math.ceil(filteredCinemas.length / itemsPerPage);

    // Get users for the current page
    const paginatedCinemas = filteredCinemas.slice(
        (currentPage - 1) * itemsPerPage,
        currentPage * itemsPerPage
    );

    // Function to change page
    const handlePageChange = (page) => {
        setCurrentPage(page);
    };

    const deleteCinemaMutation = useDeleteCinemaMutation();

    const deleteCinema = (id) => {
        SweetAlert("Are you sure?", "warning", true, null, () => {
            deleteCinemaMutation.mutate(id);
        });
    };

    const handleOpenModal = (cinema) => {
        setSelectedCinema(cinema);
        setModal(true);
    };

    const renderTable = (cinemas) => (
        <table className="w-full border-collapse border border-spacing-y-1 table-auto">
            <thead>
                <tr className="text-center bg-gray-500">
                    <th className="border py-1 px-2">Action</th>
                    <th className="border py-1 px-2">Operate</th>
                    <th className="border py-1 px-2">Cinema</th>
                    <th className="border py-1 px-2">Location</th>
                </tr>
            </thead>
            <tbody>
                {cinemas.map((cinema) => (
                    <tr key={cinema.id}>
                        <td className="border py-2 px-2">
                            <div className="flex flex-col space-y-2 justify-center h-full">
                                <button onClick={() => handleOpenModal(cinema)} className="bg-blue-500 hover:bg-blue-600 rounded-lg px-2 mx-auto">Edit</button>
                                <button onClick={() => deleteCinema(cinema.id)} className="bg-red-500 hover:bg-red-600 rounded-lg px-2 mx-auto">Delete</button>
                            </div>
                        </td>
                        <td className={`border py-1 px-2 ${cinema.operate === true ? "text-blue-500" : "text-red-500"}`}>{cinema.operate === true ? "Yes" : "No"}</td>
                        <td className="border py-1 px-2">{cinema.cinema_name}</td>
                        <td className="border py-1 px-2">{cinema.location}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    );

    return (
        <div className="text-white space-y-4">
            <div className="flex justify-between">
                <button onClick={() => handleOpenModal()} className="bg-blue-500 hover:bg-blue-600 rounded-lg block py-1.5 px-3">Add cinema</button>
                <div className="flex">
                    <span className="flex items-center relative">
                        <FaFilter className="absolute pointer-events-none left-3" /> {/* with pointer-events-none, if click icon it will click <select> (background) */}
                    </span>
                    <select value={filteredCategory} onChange={(e) => setFilteredCategory(e.target.value)} className="bg-blue-500 rounded-lg block py-1.5 pl-9 pr-2"
                    >
                        <option value="All">All</option>
                        <option value="true">Operating</option>
                        <option value="false">Not Operating</option>
                    </select>
                </div>
            </div>

            {modal ? <Modal cinema={selectedCinema} setModal={setModal} /> : null}

            <div className="overflow-auto no-select modern-scrollbar"
                ref={slider.ref}
                onMouseDown={slider.onMouseDown}
                onMouseMove={slider.onMouseMove}
                onMouseUp={slider.onMouseUp}
                onMouseLeave={slider.onMouseLeave}
            >
                {renderTable(paginatedCinemas)}
            </div>

            {filteredCinemas.length > paginatedCinemas.length &&
                <div className="flex justify-center space-x-2">
                    {Array.from({ length: totalPages }, (_, index) => (
                        <button
                            key={index}
                            onClick={() => handlePageChange(index + 1)}
                            className={`px-3 py-1 rounded ${currentPage === index + 1 ? 'bg-blue-500' : 'bg-gray-300 text-gray-800'}`}
                        >
                            {index + 1}
                        </button>
                    ))}
                </div>
            }
        </div>
    );
}

export default Cinemas;
