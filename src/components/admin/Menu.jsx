import { useEffect } from 'react';
import { Link } from "react-router-dom";
import { FaUsers, FaFilm, FaTicketAlt, FaChartLine, FaMapMarkerAlt, FaSync } from 'react-icons/fa';
import SweetAlert from "../alert/SweetAlert";
import SweetAlertFetchingData from '../alert/SweetAlertFetchingData';
import { useSyncMLQuery } from '../../config/react-query/movie';

const Menu = () => {
    const { data, error, isLoading, refetch } = useSyncMLQuery({
        enabled: false // Disable automatic query on mount
    });

    const toSyncTF = () => {
        SweetAlert("Are you sure?", "warning", true, null, () => {
            refetch();
        });
    };

    useEffect(() => {
        if (isLoading) {
            SweetAlertFetchingData("Syncing in progress...");
        } else {
            SweetAlertFetchingData.close();
        }

        if (!isLoading && data) {
            SweetAlert("Sync successful!", "success");
        }

        if (error) {
            SweetAlert(`Error: ${error.message}`, "error");
        }
    }, [isLoading, error, data]);

    return (
        <div className="flex flex-wrap justify-around w-full gap-6 py-4 px-10 shadow-inner shadow-white text-lg text-white border rounded-2xl">
            <Link to="#users" className="flex items-center">
                <FaUsers className="mr-2" />
                <p>Users</p>
            </Link>
            <Link to="#cinemas" className="flex items-center">
                <FaMapMarkerAlt className="mr-2" />
                <p>Cinemas</p>
            </Link>
            <Link to="#movies" className="flex items-center">
                <FaFilm className="mr-2" />
                <p>Movies</p>
            </Link>
            <Link to="#bookings" className="flex items-center">
                <FaTicketAlt className="mr-2" />
                <p>Bookings</p>
            </Link>
            <Link to="#statistics" className="flex items-center">
                <FaChartLine className="mr-2" />
                <p>Statistics</p>
            </Link>
            <button onClick={toSyncTF} className="flex items-center" disabled={isLoading}>
                <FaSync className="mr-2" />
                <p>Sync ML</p>
            </button>
        </div>
    );
};

export default Menu;

        // <div className="py-4 px-12 shadow-inner shadow-white space-y-4 min-w-max text-lg text-white border rounded-2xl">
        //     <Link to="#users" className="flex items-center">
        //         <FaUsers className="mr-2" />
        //         <span className="text-left">Users</span>
        //     </Link>
        //     <Link to="#cinemas" className="flex items-center">
        //         <FaMapMarkerAlt className="mr-2" />
        //         <span className="text-left">Cinemas</span>
        //     </Link>
        //     <Link to="#movies" className="flex items-center">
        //         <FaFilm className="mr-2" />
        //         <span className="text-left">Movies</span>
        //     </Link>
        //     <Link to="#bookings" className="flex items-center">
        //         <FaTicketAlt className="mr-2" />
        //         <span className="text-left">Bookings</span>
        //     </Link>
        //     <Link to="#statistics" className="flex items-center">
        //         <FaChartLine className="mr-2" />
        //         <span className="text-left">Statistics</span>
        //     </Link>
        //     <button onClick={toSyncTF} className="flex items-center" disabled={isLoading}>
        //         <FaSync className="mr-2" />
        //         <span className="text-left">Sync ML</span>
        //     </button>
        // </div>
