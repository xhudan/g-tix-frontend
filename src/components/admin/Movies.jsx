import { useState } from "react";
import { useGetAllMoviesQuery, useDeleteMovieMutation  } from "../../config/react-query/movie";
import { FaFilter } from "react-icons/fa";
import SweetAlert from "../alert/SweetAlert";
import useSlider from "../slider/Slider";

const Movies = () => {
    const [filteredCategory, setFilteredCategory] = useState("All");
    const [currentPage, setCurrentPage] = useState(1);

    const { slider } = useSlider();

    const { data } = useGetAllMoviesQuery();
    const moviesData = data?.data.data || [];

    // Sort movies by createdAt date
    const sortedMovies = [...moviesData].sort((a, b) =>
        (b.createdAt || "").localeCompare(a.createdAt || "")
    );

    const now = new Date().toLocaleDateString('en-CA');

    const nowPlayingMovies = sortedMovies.filter(
        (movie) => movie.start_date <= now && now <= movie.expired
    );

    const upcomingMovies = sortedMovies.filter(
        (movie) => movie.start_date > now
    );

    const expiredMovies = sortedMovies.filter(
        (movie) => movie.expired < now
    );

    // Filter movies based on selected category
    const filteredMovies = sortedMovies.filter((movie) => {
        if (filteredCategory === "All") {
            return true; // Show all movies if "All" is selected
        } else if (filteredCategory === "NowPlaying") {
            return nowPlayingMovies.includes(movie);
        } else if (filteredCategory === "Upcoming") {
            return upcomingMovies.includes(movie);
        } else {
            return expiredMovies.includes(movie);
        }
    });

    const itemsPerPage = 10;

    // Calculate total pages
    const totalPages = Math.ceil(filteredMovies.length / itemsPerPage);

    // Get users for the current page
    const paginatedMovies = filteredMovies.slice(
        (currentPage - 1) * itemsPerPage,
        currentPage * itemsPerPage
    );

    // Function to change page
    const handlePageChange = (page) => {
        setCurrentPage(page);
    };

    const deleteMovieMutation = useDeleteMovieMutation();

    const deleteMovie = (id) => {
        SweetAlert("Are you sure?", "warning", true, null, () => {
            deleteMovieMutation.mutate(id);
        });
    };

    const formattedTimeOptions = (time) => {
        const [hours, minutes] = time.split(':').map(Number);
        return `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}`;
    };

    const formattedTimeOptions2 = (time) => {
        if (!time) return ""; // Check for undefined or null values
        const [hours, minutes] = time.split(':').map(Number); // Mengonversi jam dan menit ke angka
        return `${hours}h ${minutes}m`;
    };

    const renderTable = (movies) => (
        <table className="w-full border-collapse border border-spacing-y-1 table-auto">
            <thead>
                <tr className="text-center bg-gray-500">
                    <th className="border py-1 px-2">Action</th>
                    <th className="border py-1 px-2">Poster</th>
                    <th className="border py-1 px-2">Title</th>
                    <th className="border py-1 px-2">Genre</th>
                    <th className="border py-1 px-2">Showtime List</th>
                    <th className="border py-1 px-2">Age</th>
                    <th className="border py-1 px-2">Duration</th>
                    <th className="border py-1 px-2">Start Date</th>
                    <th className="border py-1 px-2">Runtime</th>
                    <th className="border py-1 px-2">Price (IDR)</th>
                    <th className="border py-1 px-2">Director</th>
                    <th className="border py-1 px-2">Writer</th>
                    <th className="border py-1 px-2">Actors</th>
                    <th className="border py-1 px-2">Language</th>
                    <th className="border py-1 px-2">Synopsis</th>
                </tr>
            </thead>
            <tbody>
                {movies.map((movie) => (
                    <tr key={movie.id}>
                        <td className="border py-2 px-2">
                            <div className="flex flex-col space-y-2 justify-center h-full">
                                <button onClick={() => window.location.href = `/manage/movie/${movie.id}`} className="bg-blue-500 hover:bg-blue-600 rounded-lg px-2 mx-auto">Edit</button>
                                <button onClick={() => deleteMovie(movie.id)} className="bg-red-500 hover:bg-red-600 rounded-lg px-2 mx-auto">Delete</button>
                            </div>
                        </td>
                        <td className="border py-1 px-2">
                            <img src={movie.poster} alt={movie.title} className="h-15 mx-auto" />
                        </td>
                        <td className="border py-1 px-2">{movie.title}</td>
                        <td className="border py-1 px-2">{movie.genre?.join(", ")}</td>
                        <td className="border py-1 px-2">{movie.showtime_list?.map(formattedTimeOptions).join(", ")}</td>
                        <td className="border py-1 px-2">{movie.age}</td>
                        <td className="border py-1 px-2">{formattedTimeOptions2(movie.duration)}</td>
                        <td className="border py-1 px-2">{movie.start_date}</td>
                        <td className="border py-1 px-2">{movie.expired}</td>
                        <td className="border py-1 px-2">{movie.price?.toLocaleString("id-ID")}</td>
                        <td className="border py-1 px-2">{movie.director?.join(", ")}</td>
                        <td className="border py-1 px-2">{movie.writer?.join(", ")}</td>
                        <td className="border py-1 px-2">{movie.actors?.join(", ")}</td>
                        <td className="border py-1 px-2">{movie.language}</td>
                        <td className="border py-1 px-2">{movie.synopsis?.length > 20 ? (movie.synopsis.substring(0, 200) + "...") : movie.synopsis}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    );

    return (
        <div className="text-white space-y-4">
            <div className="flex justify-between">
                <button onClick={() => window.location.href = `/manage/movie`} className="bg-blue-500 hover:bg-blue-600 rounded-lg block py-1.5 px-3">Add movie</button>
                <div className="flex">
                    <span className="flex items-center relative">
                        <FaFilter className="absolute pointer-events-none left-3" /> {/* with pointer-events-none, if click icon it will click <select> (background) */}
                    </span>
                    <select value={filteredCategory} onChange={(e) => setFilteredCategory(e.target.value)} className="bg-blue-500 rounded-lg block py-1.5 pl-9 pr-2"
                    >
                        <option value="All">All</option>
                        <option value="NowPlaying">Now Playing</option>
                        <option value="Upcoming">Upcoming</option>
                        <option value="NotAvailable">Not Available</option>
                    </select>
                </div>
            </div>
            <div className="overflow-auto no-select modern-scrollbar"
                ref={slider.ref}
                onMouseDown={slider.onMouseDown}
                onMouseMove={slider.onMouseMove}
                onMouseUp={slider.onMouseUp}
                onMouseLeave={slider.onMouseLeave}
            >
                {renderTable(paginatedMovies)}
            </div>
            {filteredMovies.length > paginatedMovies.length &&
                <div className="flex justify-center space-x-2">
                    {Array.from({ length: totalPages }, (_, index) => (
                        <button
                            key={index}
                            onClick={() => handlePageChange(index + 1)}
                            className={`px-3 py-1 rounded ${currentPage === index + 1 ? 'bg-blue-500' : 'bg-gray-300 text-gray-800'}`}
                        >
                            {index + 1}
                        </button>
                    ))}
                </div>
            }
        </div>
    );
}

export default Movies;
