import { useEffect } from 'react';
import { useGetAllCinemasQuery } from "../../config/react-query/cinema";
import { useGetAllHistoryQuery } from "../../config/react-query/book";
import { useGetUsersQuery } from "../../config/react-query/auth";
import { useGetAllMoviesQuery } from "../../config/react-query/movie";
import SweetAlertFetchingData from '../alert/SweetAlertFetchingData';
import Movies from './statistics/Movies';
import Users from './statistics/Users';
import Visits from './statistics/Visits';
import Bookings from './statistics/Bookings';

const Statistics = () => {
    const { data: cinemas, isLoading: cinemasLoading } = useGetAllCinemasQuery();
    const { data: bookings, isLoading: bookingsLoading } = useGetAllHistoryQuery();
    const { data: users, isLoading: usersLoading } = useGetUsersQuery();
    const { data: movies, isLoading: moviesLoading } = useGetAllMoviesQuery();

    useEffect(() => {
        if (cinemasLoading || bookingsLoading || usersLoading || moviesLoading) {
            SweetAlertFetchingData("Loading...");
        } else {
            SweetAlertFetchingData.close();
        }
    }, [cinemasLoading, bookingsLoading, usersLoading, moviesLoading]);

    if (cinemasLoading || bookingsLoading || usersLoading || moviesLoading) {
        return null; // No need to render anything during loading
    }

    const usersData = users?.data.data || [];
    const bookingsData = bookings?.data.data || [];
    const cinemasData = cinemas?.data.data || [];
    const moviesData = movies?.data.data || [];

    const now = new Date().toLocaleDateString('en-CA');
    const nowPlayingMovies = moviesData.filter(
        (movie) => movie.start_date <= now && now <= movie.expired
    );

    // Filter bookings based on payment_status === "Issued"
    const issuedBookingsData = bookingsData.filter(booking => booking.payment_status === "Issued");

    // Data transformation for charts
    const activeUsersCount = usersData.filter(user => user.active).length;
    const inactiveUsersCount = usersData.length - activeUsersCount;

    const bookingCounts = issuedBookingsData.map(booking => ({
        date: booking.createdAt.split('T')[0],
        count: booking.total_booking,
    }));

    const nowPlayingBookingCounts = {};
    issuedBookingsData.forEach(booking => {
        const date = booking.createdAt.split('T')[0];
        const movie = nowPlayingMovies.find(movie => movie.id === booking.movie_id);

        if (movie) {
            if (!nowPlayingBookingCounts[date]) {
                nowPlayingBookingCounts[date] = {};
            }
            if (!nowPlayingBookingCounts[date][movie.title]) {
                nowPlayingBookingCounts[date][movie.title] = 0;
            }
            nowPlayingBookingCounts[date][movie.title] += booking.total_booking;
        }
    });

    const nowPlayingBookingData = Object.keys(nowPlayingBookingCounts).map(date => {
        const dataPoint = { date };
        nowPlayingMovies.forEach(movie => {
            dataPoint[movie.title] = nowPlayingBookingCounts[date][movie.title] || 0;
        });
        return dataPoint;
    });

    const cinemaVisits = cinemasData.map(cinema => ({
        name: cinema.cinema_name,
        visits: issuedBookingsData
            .filter(booking => booking.cinema_id === cinema.id)
            .reduce((sum, booking) => sum + booking.total_booking, 0),
    }));

    const nowPlayingVisits = cinemasData.map(cinema => ({
        name: cinema.cinema_name,
        visits: issuedBookingsData
            .filter(booking =>
                booking.cinema_id === cinema.id &&
                nowPlayingMovies.some(movie => movie.id === booking.movie_id)
            )
            .reduce((sum, booking) => sum + booking.total_booking, 0),
    }));

    const genreCounts = moviesData.reduce((acc, movie) => {
        movie.genre.forEach(genre => {
            if (!acc[genre]) acc[genre] = 0;
            acc[genre]++;
        });
        return acc;
    }, {});

    const nowPlayingGenreCounts = nowPlayingMovies.reduce((acc, movie) => {
        movie.genre.forEach(genre => {
            if (!acc[genre]) acc[genre] = 0;
            acc[genre]++;
        });
        return acc;
    }, {});

    const genreData = Object.keys(genreCounts).map(key => ({
        name: key,
        value: genreCounts[key],
    }));

    const nowPlayingGenreData = Object.keys(nowPlayingGenreCounts).map(key => ({
        name: key,
        value: nowPlayingGenreCounts[key],
    }));

    const totalNowPlayingMovies = nowPlayingMovies.length;
    const totalMovies = moviesData.length;

    // const generateColors = (numColors) => {
    //     const colors = [];
    //     for (let i = 0; i < numColors; i++) {
    //         const color = `hsl(${Math.random() * 360}, 100%, 75%)`;
    //         colors.push(color);
    //     }
    //     return colors;
    // };

    // const userColors = generateColors(2); // For active and inactive users
    // const movieColors = generateColors(totalNowPlayingMovies); // For now playing movies
    // const genreColors = generateColors(Object.keys(genreCounts).length); // For all genres

    // Determine the maximum number of items among the datasets
    const maxItems = Math.max(
        totalNowPlayingMovies,
        Object.keys(genreCounts).length,
        2 // For users (active and inactive)
    );

    const generateColors = (numColors) => {
        const colors = [];

        for (let i = 0; i < numColors; i++) {
            const color = `hsl(${Math.floor(95 * i)}, 100%, 70%)`;
            colors.push(color);
        }

        return colors;
    };

    // Generate colors based on the maximum number of items
    const colors = generateColors(maxItems);

    return (
        <div className="text-white space-y-12">
            <Users activeUsersCount={activeUsersCount} inactiveUsersCount={inactiveUsersCount} COLORS={colors} />
            <Bookings nowPlayingBookingData={nowPlayingBookingData} nowPlayingMovies={nowPlayingMovies}
                bookingCounts={bookingCounts} COLORS={colors} />
            <Visits nowPlayingVisits={nowPlayingVisits} cinemaVisits={cinemaVisits} />
            <Movies
                genreData={genreData} nowPlayingMovies={nowPlayingMovies} nowPlayingGenreData={nowPlayingGenreData}
                COLORS={colors} totalNowPlayingMovies={totalNowPlayingMovies} totalMovies={totalMovies}
            />
        </div>
    );
}

export default Statistics;
