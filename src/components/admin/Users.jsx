import { useState } from "react";
import { useGetUsersQuery } from "../../config/react-query/auth";
import { FaFilter } from "react-icons/fa";
import useSlider from "../slider/Slider";

const Users = () => {
    const [filteredCategory, setFilteredCategory] = useState("All");
    const [currentPage, setCurrentPage] = useState(1);

    const { slider } = useSlider();

    const { data } = useGetUsersQuery();
    const usersData = data?.data.data || [];

    // Sort users by createdAt date
    const sortedUsers = [...usersData].sort((a, b) =>
        (b.createdAt || "").localeCompare(a.createdAt || "")
    );

    const filteredUsers = sortedUsers.filter((user) => {
        if (filteredCategory === "All") {
            return true; // Show all users if "All" is selected
        } else if (filteredCategory === "true") {
            return user.active === true;
        } else if (filteredCategory === "false") {
            return user.active === false;
        } else {
            return user.role === filteredCategory;
        }
    });

    const itemsPerPage = 25;

    // Calculate total pages
    const totalPages = Math.ceil(filteredUsers.length / itemsPerPage);

    // Get users for the current page
    const paginatedUsers = filteredUsers.slice(
        (currentPage - 1) * itemsPerPage,
        currentPage * itemsPerPage
    );

    // Function to change page
    const handlePageChange = (page) => {
        setCurrentPage(page);
    };

    const renderTable = (users) => (
        <table className="w-full border-collapse border border-spacing-y-1 table-auto">
            <thead>
                <tr className="text-center bg-gray-500">
                    <th className="border py-1 px-2">Active</th>
                    <th className="border py-1 px-2">Full name</th>
                    <th className="border py-1 px-2">Email</th>
                    <th className="border py-1 px-2">Role</th>
                </tr>
            </thead>
            <tbody>
                {users.map((user) => (
                    <tr key={user.id}>
                        <td className={`border py-1 px-2 ${user.active === true ? "text-blue-500" : "text-red-500"}`}>{user.active === true ? "Yes" : "No"}</td>
                        <td className="border py-1 px-2">{user.full_name}</td>
                        <td className="border py-1 px-2">{user.email}</td>
                        <td className="border py-1 px-2">{user.role}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    );

    return (
        <div className="text-white space-y-4">
            <div className="flex justify-end">
                <span className="flex items-center relative">
                    <FaFilter className="absolute pointer-events-none left-3" /> {/* with pointer-events-none, if click icon it will click <select> (background) */}
                </span>
                <select
                    value={filteredCategory}
                    onChange={(e) => {
                        setFilteredCategory(e.target.value);
                        setCurrentPage(1); // Reset to the first page when filtering
                    }}
                    className="bg-blue-500 rounded-lg block py-1.5 pl-9 pr-2"
                >
                    <option value="All">All</option>
                    <option value="Admin">Admin</option>
                    <option value="User">User</option>
                    <option value="true">Activated</option>
                    <option value="false">Inactive</option>
                </select>
            </div>
            <div className="overflow-auto no-select modern-scrollbar"
                ref={slider.ref}
                onMouseDown={slider.onMouseDown}
                onMouseMove={slider.onMouseMove}
                onMouseUp={slider.onMouseUp}
                onMouseLeave={slider.onMouseLeave}
            >
                {renderTable(paginatedUsers)}
            </div>
            {filteredUsers.length > paginatedUsers.length &&
                <div className="flex justify-center space-x-2">
                    {Array.from({ length: totalPages }, (_, index) => (
                        <button
                            key={index}
                            onClick={() => handlePageChange(index + 1)}
                            className={`px-3 py-1 rounded ${currentPage === index + 1 ? 'bg-blue-500' : 'bg-gray-300 text-gray-800'}`}
                        >
                            {index + 1}
                        </button>
                    ))}
                </div>
            }
        </div>
    );
}

export default Users;
