import { useAddCinemaMutation, useUpdateCinemaMutation } from "../../../config/react-query/cinema";
import SweetAlert from "../../alert/SweetAlert";

const Modal = (props) => {
    const checkUpdateCinema = props?.cinema?.id;

    const updateCinemaMutation = useUpdateCinemaMutation(checkUpdateCinema);
    const addCinemaMutation = useAddCinemaMutation();

    const handleSubmit = (event) => {
        event.preventDefault();

        const formData = new FormData(event.target);
        const data = Object.fromEntries(formData.entries());

        if (!data.cinema_name.trim() || !data.location.trim()) {
            return SweetAlert("Please ensure all fields are filled!", "warning");
        }
        
        data.cinema_name = data.cinema_name.trim();
        data.location = data.location.trim();

        if (checkUpdateCinema) {
            updateCinemaMutation.mutate(data);
        } else {
            addCinemaMutation.mutate(data);
        }
    };

    const closeModal = async () => {
        props.setModal(false);
    };

    return (
        <div className="fixed m-auto z-[100] h-screen flex right-0 left-0 top-0 justify-center items-center">
            <div className="relative p-4 w-full max-w-md max-h-full">
                {/* Modal content */}
                <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
                    {/* Modal header */}
                    <div className="flex items-center justify-between p-4 md:p-5 border-b rounded-t dark:border-gray-600">
                        <h3 className="text-lg font-semibold text-white">
                            {checkUpdateCinema ? "Update Cinema" : "Add Cinema"}
                        </h3>
                        <button onClick={closeModal} className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white">
                            <svg className="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                                <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6" />
                            </svg>
                            <span className="sr-only">Close modal</span>
                        </button>
                    </div>
                    {/* Modal body */}
                    <form onSubmit={handleSubmit} className="p-4 md:p-5">
                        <div className="grid gap-4 mb-4 grid-cols-2">
                            <div className="col-span-2">
                                <label htmlFor="operate" className="block mb-2 text-sm font-medium text-white">Operate</label>
                                <select
                                    name="operate"
                                    defaultValue={props?.cinema?.operate ? "true" : "false"}
                                    className="border text-sm rounded-lg block w-full p-2.5 bg-gray-600 border-gray-500 placeholder-gray-400 text-white focus:ring-primary-500 focus:border-primary-500"
                                >
                                    <option value="true">Yes</option>
                                    <option value="false">No</option>
                                </select>
                            </div>
                            <div className="col-span-2">
                                <label htmlFor="cinema_name" className="block mb-2 text-sm font-medium text-white">Cinema name</label>
                                <input
                                    type="text"
                                    name="cinema_name"
                                    defaultValue={props?.cinema?.cinema_name}
                                    className="border text-sm rounded-lg block w-full p-2.5 bg-gray-600 border-gray-500 placeholder-gray-400 text-white focus:ring-primary-500 focus:border-primary-500"
                                />
                            </div>
                            <div className="col-span-2">
                                <label htmlFor="location" className="block mb-2 text-sm font-medium text-white">Location</label>
                                <textarea
                                    type="text"
                                    name="location"
                                    defaultValue={props?.cinema?.location}
                                    className="border text-sm rounded-lg block w-full p-2.5 bg-gray-600 border-gray-500 placeholder-gray-400 text-white focus:ring-primary-500 focus:border-primary-500"
                                />
                            </div>
                        </div>
                        <div className="space-x-6">
                            <button type="submit"
                                className={`text-white inline-flex items-center ${checkUpdateCinema ? "bg-red-500 hover:bg-red-600" : "bg-blue-500 hover:bg-blue-600"} focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center`}
                            >
                                Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default Modal;
