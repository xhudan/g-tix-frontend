import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import useSlider from '../../slider/Slider';

const Bookings = ({ bookingCounts, nowPlayingBookingData, nowPlayingMovies, COLORS }) => {
    const { slider } = useSlider();

    return (
        <>
            <h2 className="text-xl font-bold rounded-2xl bg-gray-500 mb-4 text-center">Bookings Over Time</h2>
            <div className="flex bg-gray-900 p-4 overflow-auto no-select modern-scrollbar"
                ref={slider.ref}
                onMouseDown={slider.onMouseDown}
                onMouseMove={slider.onMouseMove}
                onMouseUp={slider.onMouseUp}
                onMouseLeave={slider.onMouseLeave}
            >
                <div className="flex justify-center mx-auto space-x-6">
                    <div className="w-max">
                        <h3 className="bg-blue-500 rounded-2xl font-bold w-80 items-center mx-auto text-center mb-4 py-1.5">Now Playing</h3>
                        <LineChart width={768} height={400} data={nowPlayingBookingData}>
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="date" tick={{ fill: "white", fontSize: 14 }} />
                            <YAxis />
                            <Tooltip />
                            <Legend payload={[
                                { value: `Issued Bookings`, type: 'line', color: '#fff' },
                            ]} />
                            {nowPlayingMovies.map((movie, index) => (
                                <Line
                                    key={movie.title}
                                    type="monotone"
                                    dataKey={movie.title}
                                    stroke={COLORS[index % COLORS.length]}
                                    name={movie.title}
                                />
                            ))}
                        </LineChart>
                    </div>

                    <div className="min-w-max items-center flex">
                        <ul className="list-none pl-0">
                            {nowPlayingMovies.map((movie, index) => (
                                <li key={`item-${index}`} className="flex items-center mb-2 text-sm" style={{ color: COLORS[index % COLORS.length] }}>
                                    <span className="inline-block w-5 h-5 mr-2" style={{ backgroundColor: COLORS[index % COLORS.length] }}></span>
                                    {movie.title}
                                </li>
                            ))}
                        </ul>
                    </div>

                    <div className="w-max">
                        <h3 className="bg-red-500 rounded-2xl font-bold w-80 items-center mx-auto text-center mb-4 py-1.5">All Bookings</h3>
                        <LineChart width={768} height={400} data={bookingCounts}>
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="date" tick={{ fill: "white", fontSize: 14 }} />
                            <YAxis />
                            <Tooltip />
                            <Legend payload={[
                                { value: `Issued Bookings`, type: 'line', color: '#ff0' },
                            ]} />
                            <Line type="monotone" dataKey="count" stroke="#ff0" />
                        </LineChart>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Bookings;
