import { Tooltip, Legend, PieChart, Pie, Cell } from 'recharts';
import useSlider from '../../slider/Slider';

const Movies = ({ nowPlayingGenreData, genreData, totalMovies, totalNowPlayingMovies, COLORS }) => {
    const { slider } = useSlider();

    const nowPlayingColorsMap = {};
    genreData.forEach((entry, index) => {
        nowPlayingColorsMap[entry.name] = COLORS[index % COLORS.length];
    });

    return (
        <>
            <h2 className="text-xl font-bold rounded-2xl bg-gray-500 mb-4 text-center">Movie Genre Distributions</h2>
            <div className="flex bg-gray-900 p-4 overflow-auto no-select modern-scrollbar"
                ref={slider.ref}
                onMouseDown={slider.onMouseDown}
                onMouseMove={slider.onMouseMove}
                onMouseUp={slider.onMouseUp}
                onMouseLeave={slider.onMouseLeave}
            >
                <div className="flex justify-center mx-auto space-x-6">
                    <div className="w-max">
                        <h3 className="bg-blue-500 rounded-2xl font-bold text-center py-1.5">Now Playing</h3>
                        <PieChart width={400} height={400}>
                            <Pie
                                dataKey="value"
                                isAnimationActive={false}
                                data={nowPlayingGenreData}
                                cx={200}
                                cy={200}
                                outerRadius={80}
                                fill="#82ca9d"
                                label
                            >
                                {nowPlayingGenreData.map((entry, index) => (
                                    <Cell key={`cell-${index}`} fill={nowPlayingColorsMap[entry.name]} />
                                ))}
                            </Pie>
                            <Tooltip />
                            <Legend payload={[
                                { value: `Total Now Playing Movies: ${totalNowPlayingMovies}`, type: 'circle', color: '#fff' },
                            ]} />
                        </PieChart>
                    </div>

                    <div className="w-max">
                        <h3 className="bg-red-500 rounded-2xl font-bold text-center py-1.5">All Movies</h3>
                        <PieChart width={400} height={400}>
                            <Pie
                                dataKey="value"
                                isAnimationActive={false}
                                data={genreData}
                                cx={200}
                                cy={200}
                                outerRadius={80}
                                fill="#82ca9d"
                                label
                            >
                                {genreData.map((entry, index) => (
                                    <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                                ))}
                            </Pie>
                            <Tooltip />
                            <Legend payload={[
                                { value: `Total Movies: ${totalMovies}`, type: 'circle', color: '#fff' },
                            ]} />
                        </PieChart>
                    </div>

                    <div className="w-max items-center flex">
                        <ul className="list-none pl-0">
                            {genreData.map((entry, index) => (
                                <li key={`item-${index}`} className="flex items-center mb-2" style={{ color: COLORS[index % COLORS.length] }}>
                                    <span className="inline-block w-5 h-5 mr-2" style={{ backgroundColor: COLORS[index % COLORS.length] }}></span>
                                    {entry.name}
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Movies;
