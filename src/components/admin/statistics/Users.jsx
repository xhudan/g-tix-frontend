import { Tooltip, Legend, PieChart, Pie, Cell } from 'recharts';
import useSlider from '../../slider/Slider';

const Users = ({ activeUsersCount, inactiveUsersCount, COLORS }) => {
    const { slider } = useSlider();

    return (
        <>
            <h2 className="text-xl font-bold rounded-2xl bg-gray-500 mb-4 text-center">Users</h2>
            <div className="flex bg-gray-900 p-4 overflow-auto no-select modern-scrollbar"
                ref={slider.ref}
                onMouseDown={slider.onMouseDown}
                onMouseMove={slider.onMouseMove}
                onMouseUp={slider.onMouseUp}
                onMouseLeave={slider.onMouseLeave}
            >
                <div className="flex justify-center mx-auto space-x-6">
                    <div className="w-max">
                        <PieChart width={400} height={400}>
                            <Pie
                                dataKey="value"
                                isAnimationActive={false}
                                data={[
                                    { name: 'Active Users', value: activeUsersCount },
                                    { name: 'Inactive Users', value: inactiveUsersCount },
                                ]}
                                cx={200}
                                cy={200}
                                outerRadius={80}
                                fill="#8884d8"
                                label
                            >
                                {[
                                    { name: 'Active Users', value: activeUsersCount },
                                    { name: 'Inactive Users', value: inactiveUsersCount },
                                ].map((entry, index) => (
                                    <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                                ))}
                            </Pie>
                            <Tooltip />
                            <Legend />
                        </PieChart>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Users;
