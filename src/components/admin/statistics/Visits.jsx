import { XAxis, YAxis, CartesianGrid, Tooltip, Legend, BarChart, Bar } from 'recharts';
import useSlider from '../../slider/Slider';

const Visits = ({ nowPlayingVisits, cinemaVisits }) => {
    const { slider } = useSlider();

    // Menghitung panjang total nama entitas
    const totalNameLength = nowPlayingVisits.reduce((total, entity) => total + entity.name.length, 0) + (nowPlayingVisits.length - 1);

    // Mengatur lebar berdasarkan panjang total nama entitas
    const dynamicWidth = totalNameLength * 16; // Misalnya, setiap karakter memiliki lebar sekitar 10 pixel

    return (
        <>
            <h2 className="text-xl font-bold rounded-2xl bg-gray-500 mb-4 text-center">Cinema Visits</h2>
            <div className="flex bg-gray-900 p-4 overflow-auto no-select modern-scrollbar"
                ref={slider.ref}
                onMouseDown={slider.onMouseDown}
                onMouseMove={slider.onMouseMove}
                onMouseUp={slider.onMouseUp}
                onMouseLeave={slider.onMouseLeave}
            >
                <div className="flex justify-center mx-auto space-x-6">
                    <div className="w-max">
                        <h3 className="bg-blue-500 rounded-2xl font-bold w-80 items-center mx-auto text-center mb-4 py-1.5">Now Playing</h3>
                        <BarChart width={dynamicWidth} height={400} data={nowPlayingVisits}>
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="name" tick={{ fill: "white", fontSize: 14 }} />
                            <YAxis />
                            <Tooltip />
                            <Legend payload={[
                                { value: `Cinema Visits`, type: 'rect', color: '#8884d8' },
                            ]} />
                            <Bar dataKey="visits" fill="#8884d8" />
                        </BarChart>
                    </div>

                    <div className="w-max">
                        <h3 className="bg-red-500 rounded-2xl font-bold w-80 items-center mx-auto text-center mb-4 py-1.5">All Visits</h3>
                        <BarChart width={dynamicWidth} height={400} data={cinemaVisits}>
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="name" tick={{ fill: "white" }} />
                            <YAxis />
                            <Tooltip />
                            <Legend payload={[
                                { value: `Cinema Visits`, type: 'rect', color: '#8884d8' },
                            ]} />
                            <Bar dataKey="visits" fill="#8884d8" />
                        </BarChart>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Visits;
