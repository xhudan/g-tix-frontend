import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

const SweetAlert = (title, icon, showCancelButton, text, callback) => {
  const MySwal = withReactContent(Swal);
  MySwal.fire({
    title,
    icon,
    showCancelButton,
    text,
    allowEscapeKey: false,
    allowOutsideClick: false,
  }).then((result) => {
    if (result.isConfirmed) {
      // Call the callback function if provided
      if (typeof callback === "function") {
        callback();
      }
    }
  });
};

export default SweetAlert;
