import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

let mySwalInstance = null;

const SweetAlertFetchingData = (title) => {
  const MySwal = withReactContent(Swal);
  mySwalInstance = MySwal.fire({
    title,
    allowEscapeKey: false,
    allowOutsideClick: false,
    showConfirmButton: false,
    didOpen: () => {
      Swal.showLoading();
    },
  });
};

SweetAlertFetchingData.close = () => {
  if (mySwalInstance) {
    mySwalInstance.close();
  }
};

export default SweetAlertFetchingData;
