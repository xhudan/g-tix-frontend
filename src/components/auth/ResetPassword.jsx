const ResetPassword = ({ onSubmit }) => {
    const handleResetSubmit = (event) => {
        event.preventDefault();

        const formData = new FormData(event.target);
        const data = Object.fromEntries(formData.entries());

        onSubmit(data);
    };

    return (
        <div className="space-y-4 px-8 pt-8">
            <h1 className="text-2xl font-bold text-white">
                Forgot password
            </h1>
            <form className="space-y-4" onSubmit={handleResetSubmit}>
                <div>
                    <label htmlFor="email" className="block mb-2 text-sm font-medium text-white">
                        Email
                    </label>
                    <input
                        type="email"
                        name="email"
                        className="border rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white focus:ring-blue-500 focus:border-blue-500"
                        placeholder="user@example.com"
                        required={true}
                    />
                </div>
                <button
                    type="submit"
                    className="w-full text-white focus:ring-4 focus:outline-none font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-red-500 hover:bg-red-600 focus:ring-red-700"
                >
                    Submit
                </button>
            </form>
        </div>
    );
};

export default ResetPassword;
