import { useState } from 'react';
import { FaEye, FaEyeSlash } from 'react-icons/fa';

const SignIn = ({ onSubmit, toggleForm }) => {
  const [showPassword, setShowPassword] = useState(false);

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const handleSignInSubmit = (event) => {
    event.preventDefault();

    const formData = new FormData(event.target);
    const data = Object.fromEntries(formData.entries());

    onSubmit(data);
  };

  return (
    <div className="space-y-4 px-8 pt-8">
      <h1 className="text-2xl font-bold text-white">
        Sign in
      </h1>
      <form className="space-y-4" onSubmit={handleSignInSubmit}>
        <div>
          <label htmlFor="email" className="block mb-2 text-sm font-medium text-white">
            Email
          </label>
          <input
            type="email"
            name="email"
            className="border rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white focus:ring-blue-500 focus:border-blue-500"
            placeholder="user@example.com"
            required={true}
          />
        </div>
        <div>
          <label htmlFor="password" className="block mb-2 text-sm font-medium text-white">
            Password
          </label>
          <span className='flex items-center relative'>
            <input
              type={showPassword ? 'text' : 'password'}
              name="password"
              placeholder="••••••••"
              className="border rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white focus:ring-blue-500 focus:border-blue-500"
              required={true}
            />
            <button
              type="button"
              className="absolute inset-y-0 right-0 pr-3"
              onClick={togglePasswordVisibility}
            >
              {showPassword ? <FaEyeSlash className="text-white" /> : <FaEye className="text-white" />}
            </button>
          </span>
        </div>
        <button
          type="submit"
          className="w-full text-white focus:ring-4 focus:outline-none font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-blue-600 hover:bg-blue-700 focus:ring-blue-800"
        >
          Sign in
        </button>
        <div className="flex items-center justify-between">
          <button onClick={() => toggleForm('resetPassword')} className="text-sm font-medium hover:underline text-blue-500">
            Forgot password?
          </button>
        </div>
      </form>
    </div>
  );
};

export default SignIn;
