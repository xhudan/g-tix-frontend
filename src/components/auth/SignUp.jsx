import { useState } from 'react';
import { FaEye, FaEyeSlash } from 'react-icons/fa';
import SweetAlert from '../alert/SweetAlert';

const SignUp = ({ onSubmit }) => {
    const [showPassword, setShowPassword] = useState(false);
    const [showConfirmPassword, setShowConfirmPassword] = useState(false);

    const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
    };
    const toggleConfirmPasswordVisibility = () => {
        setShowConfirmPassword(!showConfirmPassword);
    };

    // const handleSignUpSubmit = (event) => {
    //     event.preventDefault();

    //     const data = {
    //         full_name: fullName,
    //         password: password,
    //         confirm_password: confirmPassword
    //     };

    //     onSubmit(data);
    // };

    const handleSignUpSubmit = (event) => {
        event.preventDefault();

        const formData = new FormData(event.target);
        const data = Object.fromEntries(formData.entries());

        if (!data.full_name.trim()) {
            return SweetAlert("Please ensure all fields are filled!", "warning");
        } else {
            data.full_name = data.full_name.trim();
            onSubmit(data);
        }
    };

    return (
        <div className="space-y-4 px-8 pt-8">
            <h1 className="text-2xl font-bold text-white">
                Register account
            </h1>
            <form className="space-y-4" onSubmit={handleSignUpSubmit}>
                <div>
                    <label htmlFor="full_name" className="block mb-2 text-sm font-medium text-white">
                        Name
                    </label>
                    <input
                        type="text"
                        name="full_name"
                        className="border rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white focus:ring-blue-500 focus:border-blue-500"
                        placeholder="Type your full name"
                    />
                </div>
                <div>
                    <label htmlFor="email" className="block mb-2 text-sm font-medium text-white">
                        Email
                    </label>
                    <input
                        type="email"
                        name="email"
                        className="border rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white focus:ring-blue-500 focus:border-blue-500"
                        placeholder="user@example.com"
                        required={true}
                    />
                </div>
                <div>
                    <label htmlFor="password" className="block mb-2 text-sm font-medium text-white">
                        Password
                    </label>
                    <span className='flex items-center relative'>
                        <input
                            type={showPassword ? 'text' : 'password'}
                            name="password"
                            placeholder="••••••••"
                            className="border rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white focus:ring-blue-500 focus:border-blue-500"
                            required={true}
                        />
                        <button
                            type="button"
                            className="absolute inset-y-0 right-0 pr-3"
                            onClick={togglePasswordVisibility}
                        >
                            {showPassword ? <FaEyeSlash className="text-white" /> : <FaEye className="text-white" />}
                        </button>
                    </span>
                </div>
                <div>
                    <label htmlFor="confirm_password" className="block mb-2 text-sm font-medium text-white">
                        Confirm password
                    </label>
                    <span className='flex items-center relative'>
                        <input
                            type={showConfirmPassword ? 'text' : 'password'}
                            name="confirm_password"
                            placeholder="••••••••"
                            className="border rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white focus:ring-blue-500 focus:border-blue-500"
                            required={true}
                        />
                        <button
                            type="button"
                            className="absolute inset-y-0 right-0 pr-3"
                            onClick={toggleConfirmPasswordVisibility}
                        >
                            {showConfirmPassword ? <FaEyeSlash className="text-white" /> : <FaEye className="text-white" />}
                        </button>
                    </span>
                </div>
                <button
                    type="submit"
                    className="w-full text-white focus:ring-4 focus:outline-none font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-red-500 hover:bg-red-600 focus:ring-red-700"
                >
                    Sign up
                </button>
            </form>
        </div>
    );
};

export default SignUp;
