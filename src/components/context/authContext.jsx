import { createContext, useContext, useState, useEffect } from "react";
import { useVerifyTokenMutation } from "../../config/react-query/auth";

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [token, setToken] = useState(() => localStorage.getItem("token") || null);
  const [email, setEmail] = useState(null);
  const [fullName, setFullName] = useState(null);
  const [userRole, setUserRole] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [hasLoaded, setHasLoaded] = useState(false);

  const verifyTokenMutation = useVerifyTokenMutation(token);

  useEffect(() => {
    const updateUserData = (decodedToken) => {
      setFullName(decodedToken.full_name);
      setEmail(decodedToken.email);
      setUserRole(decodedToken.role);
    };

    const clearUserData = () => {
      localStorage.clear();
      setToken(null);
      setFullName(null);
      setEmail(null);
      setUserRole(null);
    };

    if (token) {
      const decodedToken = JSON.parse(atob(token.split(".")[1]));
      const expirationTime = decodedToken.exp ? decodedToken.exp * 1000 : null;

      if (expirationTime && Date.now() >= expirationTime) {
        clearUserData();
      } else {
        const timeout = expirationTime ? setTimeout(() => {
          clearUserData();
        }, expirationTime - Date.now()) : null;

        if (!hasLoaded) {
          verifyTokenMutation.mutate({}, {
            onSuccess: () => {
              updateUserData(decodedToken);
              setIsLoading(false);
            },
            onError: () => {
              clearUserData();
              setIsLoading(false);
            },
          });
        }
        
        return () => {
          // Cleanup function to prevent re-running the effect
          setHasLoaded(true);
          timeout && clearTimeout(timeout);
        };
      }
    } else {
      clearUserData();
      setIsLoading(false);
    }
  }, [token, verifyTokenMutation, hasLoaded]);

  const contextValue = {
    token,
    email,
    fullName,
    userRole,
    isLoading
  };

  return (
    <AuthContext.Provider value={contextValue}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  return useContext(AuthContext);
};

// import { createContext, useContext, useState, useEffect } from "react";

// const AuthContext = createContext();

// export const AuthProvider = ({ children }) => {
//   const [token, setToken] = useState(() => {
//     return localStorage.getItem("token") || null;
//   });

//   const [email, setEmail] = useState(null);
//   const [fullName, setFullName] = useState(null);
//   const [userRole, setUserRole] = useState(null);
//   const [isLoading, setIsLoading] = useState(true);

//   useEffect(() => {
//     const updateUserData = (decodedToken) => {
//       setFullName(decodedToken.full_name);
//       setEmail(decodedToken.email);
//       setUserRole(decodedToken.role);
//     };

//     if (token) {
//       const decodedToken = JSON.parse(atob(token.split(".")[1]));
//       const expirationTime = decodedToken.exp ? decodedToken.exp * 1000 : null;

//       if (expirationTime && Date.now() >= expirationTime) {
//         localStorage.clear();
//         setToken(null);
//         setFullName(null);
//         setEmail(null);
//         setUserRole(null);
//       } else {
//         const timeout = expirationTime ? setTimeout(() => {
//           localStorage.clear();
//           setToken(null);
//           setFullName(null);
//           setEmail(null);
//           setUserRole(null);
//         }, expirationTime - Date.now()) : null;

//         updateUserData(decodedToken);

//         setIsLoading(false);

//         return () => timeout && clearTimeout(timeout);
//       }
//     } else {
//       setToken(null);
//       setFullName(null);
//       setEmail(null);
//       setUserRole(null);
//       setIsLoading(false);
//     }
//   }, [token]);

//   const contextValue = {
//     token,
//     email,
//     fullName,
//     userRole,
//     isLoading
//   };

//   return (
//     <AuthContext.Provider value={contextValue}>
//       {children}
//     </AuthContext.Provider>
//   );
// };

// export const useAuth = () => {
//   return useContext(AuthContext);
// };
