import { useState } from "react";

const TagsInput = ({ label, id, values = [], onChange, type, mandatory }) => {
    const [inputValue, setInputValue] = useState("");

    const handleKeyDown = (e) => {
        if ((e.key === "Enter" || e.key === ",") && inputValue.trim() !== "") {
            e.preventDefault();
            onChange([...values, inputValue.trim()]);
            setInputValue("");
        }
    };

    const handleRemoveTag = (index) => {
        const newValues = values.filter((_, i) => i !== index);
        onChange(newValues);
    };

    const handleChange = (e) => {
        const value = type === "time" ? formatPartialTime(e.target.value.replace(/\D/g, '')) : e.target.value;
        setInputValue(value);
    };

    const formatPartialTime = (time) => {
        const onlyNumbers = time.slice(0, 4);
        const hours = onlyNumbers.slice(0, 2);
        const minutes = onlyNumbers.slice(2, 4);
        let formattedTime = hours;

        if (minutes.length > 0) {
            formattedTime += `:${minutes}`;
        }

        if (hours && (parseInt(hours) > 23 || parseInt(hours) < 0)) {
            return "";
        }
        if (minutes && (parseInt(minutes) > 59 || parseInt(minutes) < 0)) {
            return "";
        }

        return formattedTime;
    };

    return (
        <div className="relative mb-2">
            <div className="block px-3 h-full pb-1 pt-3 w-full bg-transparent rounded-lg border border-gray-400 peer focus-within:border-red-500 focus-within:outline-none">
                <div className="flex flex-wrap">
                    {values?.map((value, index) => (
                        <span key={index} className="inline-flex items-center mr-2 mb-2 px-2 py-1 rounded-full bg-blue-500 text-white">
                            {value}
                            <button type="button" onClick={() => handleRemoveTag(index)} className="ml-2 text-xs">
                                ✕
                            </button>
                        </span>
                    ))}
                    <input
                        id={id}
                        type="text"
                        className="peer bg-transparent w-36 mb-2 flex-grow outline-none"
                        value={inputValue}
                        onChange={handleChange}
                        onKeyDown={handleKeyDown}
                        placeholder={values.length > 0 ? `Add ${label.toLowerCase()}` : ""}
                    />
                    <label
                        htmlFor={id}
                        className={`absolute rounded-sm text-gray-400 bg-neutral-900 duration-300 transform z-10 origin-[0] left-1 ${
                            inputValue || values.length > 0 ? '-translate-y-5 scale-75 top-2 w-max px-2' : 'top-1/2 -translate-y-1/2 scale-100 mx-2'
                        } peer-focus:scale-75 peer-focus:-translate-y-5 peer-focus:top-2 peer-focus:w-max peer-focus:px-2 peer-focus:mx-0`}
                    >
                        {label} {type === "time" ? "(HH:MM format)" : "(press Enter or comma to add)"}{mandatory && <span className="text-red-500">*</span>}
                    </label>
                </div>
            </div>
        </div>
    );
};

export default TagsInput;
