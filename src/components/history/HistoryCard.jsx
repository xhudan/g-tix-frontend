import { Link } from "react-router-dom";

const HistoryCard = (props) => {
    const formatDate = (value) => {
        const date = new Date(value);
        const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        const filterDate = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
        const format = `${monthNames[date.getMonth()]} ${filterDate}, ${date.getFullYear()}`;
        return format;
    };

    const formattedTimeOptions = (time) => {
        // const [hours, minutes, seconds] = time.split(':');
        const [hours, minutes] = time.split(':');
        return `${hours}:${minutes}`;
    };

    const { cancelBooking, makePayment } = props; // Destructure cancelBooking & makePayment from props

    return (
        <div className="flex flex-col items-center mx-auto">
            {props.items?.map((item) => (
                <div key={item.id} className="bg-amber-500 flex rounded-2xl mb-5 w-full sm:w-max">
                    <div className="p-4 w-96 space-y-4 m-auto justify-center rounded-2xl bg-white">
                        <div className="flex justify-between space-x-2"> {/* Add items-center to align */}
                            <Link to={`/movie/${item.movie?.id}`} draggable={false}>
                                <span className="text-lg font-bold">{item.movie?.title}</span>
                            </Link>
                            <span className="text-sm">{item.cinema?.location}</span>
                        </div>
                        {item.seats.length > 0 && (
                            <div className="space-y-4">
                                <div className="flex justify-between space-x-2">
                                    <span className="text-xs text-gray-600">Date:</span>
                                    <span className="text-xs font-bold">{formatDate(item.seats[0]?.datepick)}</span>
                                </div>
                                <div className="flex justify-between space-x-2">
                                    <span className="text-xs text-gray-600">Showtime:</span>
                                    <span className="text-xs font-bold">{formattedTimeOptions(item.seats[0]?.showtime)}</span>
                                </div>
                            </div>
                        )}
                        <div className="flex justify-between space-x-2">
                            <span className="text-xs text-gray-600">Seat Number:</span>
                            <span className="text-xs font-bold">
                                {item.seats?.some(seat => seat.seat_number === null) ? "Cancelled" : item.seats.map(seat => seat.seat_number).sort((a, b) => a - b).join(', ')}
                            </span>
                        </div>
                        <div className="flex justify-between space-x-2">
                            <span className="text-xs text-gray-600">Quantity:</span>
                            <span className="text-xs font-bold">{item.total_booking}</span>
                        </div>
                        <div className="flex justify-between space-x-2">
                            <span className="text-xs text-gray-600">Total Price:</span>
                            <span className="text-xs font-bold">Rp{item.total_price?.toLocaleString("id-ID")},00</span>
                        </div>
                        <div className="flex justify-between space-x-2">
                            <span className="text-xs font-bold">{item.booking_code}</span>
                            <span className={`text-xs font-bold px-2 py-1 rounded text-white ${item.payment_status === 'Pending' ? 'bg-red-600' :
                                item.payment_status === 'Issued' ? 'bg-blue-600' : 'bg-gray-500'}`}>{item.payment_status}</span>
                        </div>
                        {item.payment_status === "Pending" &&
                            //if there is no cancel button <div className="mt-4 items-end flex flex-col">
                            <div className="flex justify-between space-x-2">
                                <button onClick={() => cancelBooking(item.id)} className="rounded-xl font-bold p-2 w-24 bg-gray-500 hover:bg-gray-600 text-white">Cancel</button>
                                <button onClick={() => makePayment(item.booking_code)} className="rounded-xl font-bold p-2 w-24 bg-red-500 hover:bg-red-600 text-white">Pay</button>
                            </div>
                        }
                    </div>
                    <div className="m-auto justify-center">
                        <Link to={`/movie/${item.id}`} draggable={false}>
                            <img src={item.movie?.poster} alt={item.movie?.title} draggable={false}
                                className="max-h-[260px] rounded-2xl" />
                        </Link>
                    </div>
                </div>
            ))}
        </div>
    );
};

export default HistoryCard;
