const MovieId = (props) => {
  return (
    <div
      key={props.movie.id}
      className="text-white w-[250px]"
    >
      <img src={props.movie.poster} alt={props.movie.title} draggable={false}
        className="max-h-[350px] mx-auto items-center object-fill"
      />
      <div className="mb-5">
        {/* <h5 className="rounded-xl bg-[#19376D] mt-2 text-xl font-bold text-white text-center">
              4.5 <FaStar className="inline" color="gold" />
            </h5> */}
        <h5 className="rounded-xl bg-[#19376D] mt-2 px-6 text-xl font-bold text-white text-center">
          Rp{props.movie.price?.toLocaleString("id-ID")},00
        </h5>
      </div>
    </div>
  );
};

export default MovieId;
