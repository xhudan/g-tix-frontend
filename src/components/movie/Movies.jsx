import { Link } from "react-router-dom";
import useSlider from "../slider/Slider";

const Movies = (props) => {
  const { slider } = useSlider();

  return (
    // Replace this to line 10-16 to split items into 4 grids with items extending downwards and overflow-x is not applicable
    // <div className="grid h-full gap-8 my-12 grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 place-items-center">
    <div className="my-6 overflow-x-auto no-select modern-scrollbar"
      ref={slider.ref}
      onMouseDown={slider.onMouseDown}
      onMouseMove={slider.onMouseMove}
      onMouseUp={slider.onMouseUp}
      onMouseLeave={slider.onMouseLeave}
    >
      <div className="flex justify-center w-max mx-auto space-x-6">
        {props.items?.map((item) => (
          <Link key={item.id} to={`movie/${item.id}`} draggable={false}>
            <div className="w-[200px] duration-200 flex-grow transition-colors">
              <img
                src={item.poster}
                className="max-h-[250px] mx-auto items-center object-fill"
                draggable={false}
                alt={item.title}
              />
              <div className="h-[80px] px-4 mb-5">
                <h5 className="mt-2 line-clamp-3 hover:line-clamp-none hover:bg-neutral-900 relative text-lg font-bold text-white text-center">
                  {/* 'relative' to make 'hover:bg-neutral-900' on the top layer, above item.age */}
                  {item.title}
                </h5>
              </div>
              {item.age &&
                <h5 className="rounded-xl bg-[#19376D] my-3 text-white text-center">
                  {item.age}+
                </h5>
              }
            </div>
          </Link>
        ))}
      </div>
    </div>
  );
};

export default Movies;
