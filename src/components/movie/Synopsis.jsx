import { useState } from "react";
import useSlider from "../slider/MultipleSlider";

const Synopsis = (props) => {
  const { createSlider } = useSlider();

  const cinemaSlider = createSlider();
  const dateSlider = createSlider();
  const timeSlider = createSlider();

  const formatDate = (value) => {
    const date = new Date(value);
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    const filterDate = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
    const format = `${monthNames[date.getMonth()]} ${filterDate}, ${date.getFullYear()}`;
    return format;
  };

  const timeOptions = props.movie.showtime_list;

  const formattedTimeOptions = timeOptions?.map((time) => {
    // const [hours, minutes, seconds] = time.split(':');
    const [hours, minutes] = time.split(':');
    return `${hours}:${minutes}`;
  });

  // Function to generate all dates from now to expired
  const generateDates = (startDate, endDate) => {
    const dates = [];
    const currentDate = new Date(startDate);
    const lastDate = new Date(endDate);

    // Set both dates to midnight to ensure accurate comparison, as the currentDate variable is not initialized to 0,0,0,0.
    currentDate.setHours(0, 0, 0, 0);
    lastDate.setHours(0, 0, 0, 0);

    while (currentDate <= lastDate) {
      dates.push({
        date: new Date(currentDate),
        day: currentDate.toLocaleDateString('en-US', { weekday: 'long' }) // Get the day for the current date
      });
      currentDate.setDate(currentDate.getDate() + 1);
    }

    return dates;
  };

  // Get all dates from now to expired with corresponding days
  const allDates = generateDates(new Date(), new Date(props.movie.expired));
  const [selectedCinema, setSelectedCinema] = useState(null);
  const [selectedDate, setSelectedDate] = useState(null);
  const [selectedTime, setSelectedTime] = useState(null);

  const handleCinemaSelect = (cinemaId) => {
    setSelectedCinema(cinemaId);
  };

  const handleDateSelect = (date) => {
    setSelectedDate(date);
    setSelectedTime(null); // Reset selected time when changing date
  };

  const handleTimeSelect = (time) => {
    setSelectedTime(time);
  };

  const isAllSelected = selectedCinema && selectedDate && selectedTime;

  const checkSeat = () => {
    const getDateOnly = selectedDate.toLocaleDateString('en-CA'); // CA will format the selected date as yyyy-mm-dd
    window.location.href = `/check/${props.movie.id}?cid=${selectedCinema}&d=${getDateOnly}&t=${selectedTime}:00`;
  };

  // Function to check if the selected time is in the past
  const isPastTime = (time) => {
    const [selectedHour, selectedMinute] = time.split(':').map(Number);
    const now = new Date();
    const currentHour = now.getHours();
    const currentMinute = now.getMinutes();
    return now.toDateString() === selectedDate.toDateString() && (currentHour > selectedHour || (currentHour === selectedHour && currentMinute > selectedMinute));
  };

  const formattedTimeOptions2 = (time) => {
    if (!time) return ""; // Check for undefined or null values
    const [hours, minutes] = time.split(':').map(Number); // Mengonversi jam dan menit ke angka
    return `${hours}h ${minutes}m`;
  };

  return (
    <div>
      <div className="text-white">
        <h1 className="mb-6 text-white text-xl font-bold">{props.movie?.title}</h1>
        <table className="border-separate border-spacing-y-1 table-auto">
          <tbody>
            <tr>
              <td>Genre</td>
              <td>:</td>
              <td className="pl-2 font-bold">{props.movie.genre?.join(", ")}</td>
            </tr>
            <tr>
              <td>Director</td>
              <td>:</td>
              <td className="pl-2 font-bold">{props.movie.director?.join(", ")}</td>
            </tr>
            <tr>
              <td>Writer</td>
              <td>:</td>
              <td className="pl-2 font-bold">{props.movie.writer?.join(", ")}</td>
            </tr>
            <tr>
              <td>Actors:</td>
              <td>:</td>
              <td className="pl-2 font-bold">{props.movie.actors?.join(", ")}</td>
            </tr>
            <tr>
              <td>Cencor Rating</td>
              <td>:</td>
              {props.movie.age && <td className="pl-2 font-bold">{props.movie.age}+</td>}
            </tr>
            <tr>
              <td>Duration</td>
              <td>:</td>
              <td className="pl-2 font-bold">{formattedTimeOptions2(props.movie?.duration)}</td>
            </tr>
            <tr>
              <td>Language</td>
              <td>:</td>
              <td className="pl-2 font-bold">{props.movie?.language}</td>
            </tr>
          </tbody>
        </table>

        <h1 className="mt-12 text-xl font-bold">Synopsis</h1>
        <h5 className="mt-6 text-justify">
          {props.movie.synopsis}
        </h5>
      </div>

      {props.isPlaying === "NOW PLAYING" && <>
        <h1 className="mt-12 mb-6 text-white text-xl font-bold">Select Cinema</h1>
        <div className="flex space-x-6 overflow-auto no-select modern-scrollbar"
          ref={cinemaSlider.ref}
          onMouseDown={cinemaSlider.onMouseDown}
          onMouseMove={cinemaSlider.onMouseMove}
          onMouseUp={cinemaSlider.onMouseUp}
          onMouseLeave={cinemaSlider.onMouseLeave}
        >
          {props.cinema?.map((item) => (
            <div key={item.id}
              className={`min-w-52 divide-y divide-black px-4 mb-5 rounded-xl text-center bg-${selectedCinema === item.id ? 'blue-500' : 'white'} cursor-pointer flex flex-col justify-center`}
              onClick={() => handleCinemaSelect(item.id)}>
              <h5 className="py-2 font-bold">
                {item.cinema_name}
              </h5>
              <h5 className="py-2 text-center">
                {item.location}
              </h5>
            </div>
          ))}
        </div>
        <h1 className="mt-12 mb-6 text-white text-xl font-bold">Select Date</h1>
        <div className="flex space-x-6 overflow-auto no-select modern-scrollbar"
          ref={dateSlider.ref}
          onMouseDown={dateSlider.onMouseDown}
          onMouseMove={dateSlider.onMouseMove}
          onMouseUp={dateSlider.onMouseUp}
          onMouseLeave={dateSlider.onMouseLeave}
        >
          {allDates.map((dateObj, index) => (
            <div key={index}
              className={`min-w-36 divide-y divide-black px-4 mb-5 rounded-xl text-center bg-${selectedDate && selectedDate.toDateString() === dateObj.date.toDateString() ? 'blue-500' : 'white'} cursor-pointer flex flex-col justify-center`}
              onClick={() => handleDateSelect(dateObj.date)}>
              <h5 className="py-2 font-bold">
                {dateObj.day}
              </h5>
              <h5 className="py-2">
                {formatDate(dateObj.date)}
              </h5>
            </div>
          ))}
        </div>
        <h1 className="mt-12 mb-6 text-white text-xl font-bold">Select Time</h1>
        <div className="flex space-x-6 overflow-auto no-select modern-scrollbar"
          ref={timeSlider.ref}
          onMouseDown={timeSlider.onMouseDown}
          onMouseMove={timeSlider.onMouseMove}
          onMouseUp={timeSlider.onMouseUp}
          onMouseLeave={timeSlider.onMouseLeave}
        >
          {formattedTimeOptions?.map((time, index) => (
            <div key={index}
              className={`min-w-24 px-4 mb-5 rounded-xl text-center cursor-pointer bg-${selectedTime === time ? 'blue-500' : (selectedDate && !isPastTime(time)) ? 'white' : 'gray-500 cursor-not-allowed'} flex flex-col justify-center`}
              onClick={() => selectedDate && !isPastTime(time) && handleTimeSelect(time)}
            >
              <h5 className="py-2 font-bold">{time}</h5>
            </div>
          ))}
        </div>
        <div className="grid grid-cols-1 place-items-center">
          <button className={`w-52 mt-12 font-bold p-3 text-center text-white rounded-xl ${isAllSelected ? 'bg-red-500 hover:bg-red-600' : 'bg-[#9E9E9E] bg-gray-500'}`}
            disabled={!isAllSelected}
            onClick={checkSeat}>
            Check Seat
          </button>
        </div>
      </>}
    </div>
  );
};

export default Synopsis;
