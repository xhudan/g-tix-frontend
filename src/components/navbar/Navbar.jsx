import { Link } from "react-router-dom";
import { useState } from 'react';
import { FaUser, FaBell } from 'react-icons/fa';
import gtixLogo from "../../assets/G-Tix_600.png";
import { useAuth } from "../context/authContext";
import Profile from "./Profile";
import Notification from "./Notification";

const Navbar = () => {
  const { token, email, fullName, userRole } = useAuth();
  const [isProfileOpen, setProfileOpen] = useState(false);
  const [notifications, setNotifications] = useState([]);

  const toggleProfile = () => {
    setProfileOpen(!isProfileOpen);
  };

  const showNotification = (message) => {
    const id = Date.now();
    setNotifications([...notifications, { id, message }]);
    setTimeout(() => {
      setNotifications((prevNotifications) => prevNotifications.filter((notification) => notification.id !== id));
    }, 2000);
  };

  const handleNotificationClick = () => {
    showNotification("Comingsoon");
  };

  return (
    <nav className="bg-sky-900 min-w-max">
      <div className="mx-auto sm:mx-10 flex py-1 space-x-6 justify-between items-center">
        <Link to="/">
          <img src={gtixLogo} onContextMenu={(e) => e.preventDefault()} className="ml-6 min-w-32 max-w-32" alt="Profile" />
        </Link>
        {token ? (
          <div className="space-x-1 relative">
            {userRole === "Admin" && (
              <button onClick={() => window.location.href="/manage"} className="text-white font-bold mr-4">
                Admin
              </button>
            )}
            <button onClick={handleNotificationClick} className="p-4 text-white">
              <FaBell />
            </button>
            <button onClick={toggleProfile} className="p-4 pr-6 text-white">
              <FaUser />
            </button>
            {isProfileOpen && <Profile fullName={fullName} email={email} />}
            <Notification notifications={notifications} />
          </div>
        ) : (
          <div className="text-white space-x-6 my-3">
            <Link to="/auth" className="bg-red-400 hover:bg-red-500 rounded-xl px-2 mr-6 py-1">
              Sign in
            </Link>
          </div>
        )}
      </div>
    </nav>
  );
};

export default Navbar;
