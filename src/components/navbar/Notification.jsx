const Notification = ({ notifications }) => {
  return (
    <div className="fixed top-4 left-1/2 transform -translate-x-1/2 space-y-2 z-50">
      {notifications.map((notification) => (
        <div
          key={notification.id}
          className="bg-gray-800 text-white px-4 py-2 rounded shadow-md animate-bounce"
        >
          {notification.message}
        </div>
      ))}
    </div>
  );
};

export default Notification;
