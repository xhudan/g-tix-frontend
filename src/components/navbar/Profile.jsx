import { useLogoutMutation } from '../../config/react-query/auth';

const Profile = (props) => {
    const useLogout = useLogoutMutation();

    const logout = () => {
      useLogout.mutateAsync();
    };

    const history = () => {
      window.location.replace("/history");
    };
    
    const editProfile = () => {
      window.location.replace("/profile");
    };
    
    return (
        <div className="bg-white z-50 absolute top-full w-max right-5 py-4 rounded shadow">
            <p className="px-4 font-bold">{props.fullName.length > 20 ? (props.fullName.substring(0, 17) + "...") : props.fullName}</p>
            <p className="px-4">{props.email.length > 20 ? (props.email.substring(0, 17) + "...") : props.email}</p>
            <hr className="px-4 my-2" />
            <button onClick={history} className="text-gray-800 block w-full text-left hover:bg-gray-200 px-4 py-2">
                History Booking
            </button>
            <button onClick={editProfile} className="text-gray-800 block w-full text-left hover:bg-gray-200 px-4 py-2">
                Account Settings
            </button>
            <button onClick={logout} className="text-gray-800 block w-full text-left hover:bg-gray-200 px-4 py-2">
                Sign Out
            </button>
        </div>
    );
}

export default Profile;
