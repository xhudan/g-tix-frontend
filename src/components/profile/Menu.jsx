import { FaUserCircle, FaLock } from 'react-icons/fa';
import { Link } from 'react-router-dom';

const Menu = ({isPassword}) => {
    return (
        <div className="py-4 px-12 shadow-inner shadow-white space-y-4 min-w-max text-lg text-white border rounded-2xl">
            <Link to="#user" className="flex items-center">
                <FaUserCircle className="mr-2" />
                <span className="text-left">Your Profile</span>
            </Link>
            <Link to="#security" className="flex items-center">
                <FaLock className="mr-2" />
                <span className="text-left">{isPassword ? "Change Password" : "Set Password"}</span>
            </Link>
        </div>
    );
};

export default Menu;
