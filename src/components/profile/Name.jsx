import { useState } from "react";
import { useAuth } from "../context/authContext";
import { useUpdateUserMutation } from "../../config/react-query/auth";
import SweetAlert from "../alert/SweetAlert";

const Name = () => {
    const { email, fullName } = useAuth();
    const updateUserMutation = useUpdateUserMutation();
    const [newFullName, setNewFullName] = useState("");

    const onSubmit = async (e) => {
        e.preventDefault();
        if (newFullName.trim().length > 50) {
            SweetAlert("Max input is 50 characters!", "error");
            return
        }
        updateUserMutation.mutateAsync({ full_name: newFullName.trim() });
    };

    return (
        <div className="border shadow-inner shadow-white py-8 px-12 rounded-2xl min-w-72">
            <form onSubmit={onSubmit}>
                <div className="lg:max-w-[400px]">
                    <p className="text-white text-justify">Email</p>
                    <input className="mt-2 text-white font-bold border rounded-lg py-1.5 px-2 w-full" defaultValue={email} disabled={true} />
                    <p className="mt-4 text-white text-justify">Full name</p>
                    <input className="mt-2 text-white font-bold bg-transparent border rounded-lg py-1.5 px-2 w-full" name="full_name" defaultValue={fullName} onChange={(e) => setNewFullName(e.target.value)} />
                </div>
                <button type="submit" disabled={!newFullName.trim()}
                    className={`mt-4 min-w-24 w-max text-white rounded-lg px-5 py-2 text-center ${!newFullName.trim() ? "bg-gray-500" : "bg-red-500 hover:bg-red-600"}`}
                > Save
                </button>
            </form>
        </div>
    );
};

export default Name;
