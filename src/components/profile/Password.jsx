import { useUpdateUserMutation } from "../../config/react-query/auth";
import { FaEye, FaEyeSlash } from 'react-icons/fa';
import { useState } from 'react';

const Password = ({ isPassword }) => {
    const [showNewPassword, setShowNewPassword] = useState(false);
    const [showConfirmPassword, setShowConfirmPassword] = useState(false);

    const toggleNewPasswordVisibility = () => {
        setShowNewPassword(!showNewPassword);
    };
    const toggleConfirmPasswordVisibility = () => {
        setShowConfirmPassword(!showConfirmPassword);
    };

    const updateUserMutation = useUpdateUserMutation();

    const onSubmit = async (e) => {
        e.preventDefault();
        const formData = new FormData(e.target);
        const data = Object.fromEntries(formData.entries());

        updateUserMutation.mutateAsync(data);
    };

    return (
        <div className="border shadow-inner shadow-white py-8 px-12 rounded-2xl min-w-72">
            <form onSubmit={onSubmit}>
                <div className="lg:max-w-[400px]">
                    {isPassword &&
                        <>
                            <p className="mb-2 text-white text-justify">Current password</p>
                            <input className="text-white font-bold bg-transparent border rounded-lg py-1.5 px-2 w-full" name="password" placeholder="••••••••" type="password" required={true} />
                        </>
                    }

                    <p className="mt-4 mb-2 text-white text-justify">New password</p>
                    <span className='flex items-center relative'>
                        <input className="text-white font-bold bg-transparent border rounded-lg py-1.5 px-2 w-full" name="new_password" placeholder="••••••••" type={showNewPassword ? 'text' : 'password'} required={true} />
                        <button type="button" className="absolute inset-y-0 right-0 pr-3" onClick={toggleNewPasswordVisibility} >
                            {showNewPassword ? <FaEyeSlash className="text-white" /> : <FaEye className="text-white" />}
                        </button>
                    </span>

                    <p className="mt-4 mb-2 text-white text-justify">Confirm new password</p>
                    <span className='flex items-center relative'>
                        <input className="text-white font-bold bg-transparent border rounded-lg py-1.5 px-2 w-full" name="confirm_new_password" placeholder="••••••••" type={showConfirmPassword ? 'text' : 'password'} required={true} />
                        <button type="button" className="absolute inset-y-0 right-0 pr-3" onClick={toggleConfirmPasswordVisibility} >
                            {showConfirmPassword ? <FaEyeSlash className="text-white" /> : <FaEye className="text-white" />}
                        </button>
                    </span>
                </div>
                <button
                    type="submit"
                    className="mt-4 text-white font-medium rounded-lg px-5 py-2 text-center bg-red-500 hover:bg-red-600"
                >
                    {isPassword ? "Change Password" : "Set Password"}
                </button>
            </form>
        </div>
    );
};

export default Password;
