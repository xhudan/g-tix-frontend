import { useState, useEffect } from "react";
import { useAddBookingMutation } from "../../config/react-query/book";
import useSlider from "../slider/Slider";

const SeatPicker = ({ seatChecker, moviePrice, movieId, cid, d, t }) => {
  const [bookedSeats, setBookedSeats] = useState([]);
  const [selectedSeats, setSelectedSeats] = useState([]);

  const { slider } = useSlider();

  // Updating bookedSeats every time the seatChecker changes
  useEffect(() => {
    if (seatChecker) {
      const bookedSeatNumbers = seatChecker.map(seat => seat.seat_number);
      setBookedSeats(bookedSeatNumbers);
    }
  }, [seatChecker]);

  const handleSeatSelect = (seat) => {
    if (selectedSeats.includes(seat)) {
      setSelectedSeats(selectedSeats.filter(selectedSeat => selectedSeat !== seat));
    } else {
      setSelectedSeats([...selectedSeats, seat]);
    }
  };

  const addBookingMutation = useAddBookingMutation();

  const addBooking = () => {
    const seatsData = selectedSeats.map(seatNumber => ({ seat_number: seatNumber }));
    addBookingMutation.mutate({
      movieId: movieId,
      seats: seatsData,
      cinema_id: cid,
      date: d,
      time: t
    });
  };

  const renderSeats = () => {
    const rows = [];
    for (let row = 1; row <= 10; row++) {
      const seatsInRow = [];
      for (let col = 1; col <= 9; col++) {
        const seatNumber = (col - 1) * 10 + row;
        seatsInRow.push(
          <div
            key={seatNumber}
            className={`min-w-10 min-h-10 md:w-12 md:h-12 m-2 flex items-center justify-center rounded ${bookedSeats.includes(seatNumber) ? "bg-gray-500 text-white" : selectedSeats.includes(seatNumber) ? "cursor-pointer bg-blue-500 text-white" : "cursor-pointer bg-white"
              }`}
            onClick={() => handleSeatSelect(seatNumber)}
          >
            {seatNumber}
          </div>
        );
        // Add space after every third seat
        if (col % 3 === 0 && col !== 9) {
          seatsInRow.push(<div key={`space-${col}`} className="min-w-10" />);
        }
      }
      rows.push(
        <div key={`row-${row}`} className="flex items-center justify-center">
          {seatsInRow}
        </div>
      );
    }
    return rows;
  };

  const totalPrice = selectedSeats.length * moviePrice;
  const formattedPrice = totalPrice.toLocaleString("id-ID");//Adds a dot after every three digits

  return (
    <div className="text-center">
      <h1 className="text-2xl text-white font-bold mt-6 mb-4">Select Seats</h1>
      <div className="overflow-auto no-select modern-scrollbar"
        ref={slider.ref}
        onMouseDown={slider.onMouseDown}
        onMouseMove={slider.onMouseMove}
        onMouseUp={slider.onMouseUp}
        onMouseLeave={slider.onMouseLeave}
      >
        <div className="w-max mx-auto">
          {renderSeats()}
        </div>
      </div>
      <div className="flex space-x-6 my-4 justify-center">
        <button className={`items-center flex px-4 py-2 text-white rounded-xl bg-${!selectedSeats.length ? 'gray-500' : 'red-500 hover:bg-red-600'}`}
          onClick={addBooking} disabled={!selectedSeats.length}>
          Book Selected Seats
        </button>
        <div className="items-center flex px-4 py-2 bg-gray-500 text-white rounded-xl">
          Price: Rp{formattedPrice},00
        </div>
      </div>
    </div>
  );
};

export default SeatPicker;
