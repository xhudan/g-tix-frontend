import axios from "axios";

const token = localStorage.getItem("token");

const API = axios.create({
  baseURL: process.env.REACT_APP_BASE_API,
  headers: {
    Accept: "application/json",
    Authorization: token ? `Bearer ${token}` : null,
    },
  },
);

export default API;
