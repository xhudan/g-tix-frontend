import { useMutation, useQueryClient, useQuery } from 'react-query';
import SweetAlert from '../../components/alert/SweetAlert';
import API from '../api/baseApi';

export const useVerifyTokenMutation = () => {
    return useMutation(
        () => API.post('/auth/verify-token')
    );
};

export const useGetUsersQuery = () => {
    return useQuery(`users`, () => API.get(`/auth/users`), {
        retry: (failureCount, error) => {
            // if error is 'Invalid token', don't retry
            if (error.response && error.response.data && error.response.data.msg === 'Invalid token') {
                return false;
            }
            // Default retry behavior for other errors
            return true;
        },
        onError: (error) => {
            if (error.response && error.response.data && error.response.data.msg === 'Invalid token') {
                localStorage.clear();
                window.location.replace("/auth");
            }
        }
    });
};

export const useOAuthMutation = () => {
    return useMutation(
        (data) => API.post('/auth/google/callback', data),
        {
            onSuccess: (response) => {
                localStorage.setItem('token', response.data.token);
                window.location.replace('/');
            },
            onError: (error) => {
                const message =
                    (error.response && error.response.data && error.response.data.msg) ||
                    error.msg || error.toString();
                SweetAlert(message, 'error');
            },
        }
    );
};

export const useLoginMutation = () => {
    const resendOTP = useResendOTPMutation(); // Call useResendOTP outside useLoginMutation

    return useMutation(
        (data) => API.post('/auth/login', data),
        {
            onSuccess: (response) => {
                localStorage.setItem('token', response.data.token);
                window.location.replace('/');
            },
            onError: async (error, data) => {
                const message = error.response?.data?.msg || error.msg || error.toString();
                if (message === "Account is not activated") {
                    SweetAlert("Account is not activated", 'error', true, "Would you like to resend the activation email?", async () => {
                        const email = data?.email;
                        if (email) {
                            await resendOTP.mutateAsync({ email }); // Use the mutateAsync function to perform mutations asynchronously
                        }
                    });
                } else {
                    SweetAlert(message, 'error');
                }
            },
        }
    );
};

export const useVerifyOTPMutation = (type, queryValue) => {
    return useMutation(
        (data) => {
            if (type === 'reset' && data) {
                return API.post(`/auth/verify?${type}=${queryValue}`, data);
            } else {
                return API.post(`/auth/verify?${type}=${queryValue}`);
            }
        }
    );
};

export const useResendOTPMutation = () => {
    return useMutation(
        (data) => API.post('/auth/send-OTP', data),
        {
            onSuccess: (response) => {
                SweetAlert(response.data.msg, "success");
            },
            onError: (error) => {
                const message =
                    (error.response && error.response.data && error.response.data.msg) ||
                    error.msg || error.toString();
                SweetAlert(message, 'error');
            },
        }
    );
};

export const useResetPasswordMutation = () => {
    return useMutation(
        (data) => API.post('/auth/reset-password', data),
        {
            onSuccess: (response) => {
                SweetAlert(response.data.msg, "success", false, null, () => {
                    window.location.reload();
                });
            },
            onError: (error) => {
                const message =
                    (error.response && error.response.data && error.response.data.msg) ||
                    error.msg || error.toString();
                SweetAlert(message, 'error');
            },
        }
    );
};

export const useRegisterMutation = () => {
    return useMutation(
        (data) => API.post('/auth/register', data),
        {
            onSuccess: (response) => {
                SweetAlert(response.data.msg, "success", false, null, () => {
                    window.location.reload();
                });
            },
            onError: (error) => {
                const message =
                    (error.response && error.response.data && error.response.data.msg) ||
                    error.msg || error.toString();
                const messageDetail =
                    (error.response && error.response.data && error.response.data.msgDetail) ||
                    error.msgDetail || null;
                SweetAlert(message, 'error', false, messageDetail);
            },
        }
    );
};

export const useUpdateUserMutation = () => {
    return useMutation(
        (data) => API.put('/auth/update-user', data),
        {
            onSuccess: (response) => {
                localStorage.setItem('token', response.data.token);
                SweetAlert(response.data.msg, 'success', false, null, () => {
                    window.location.reload();
                });
            },
            onError: (error) => {
                const message =
                    (error.response && error.response.data && error.response.data.msg) ||
                    error.msg || error.toString();
                const messageDetail =
                    (error.response && error.response.data && error.response.data.msgDetail) ||
                    error.msgDetail || null;

                if (message === 'Invalid token') {
                    localStorage.clear();
                }
                SweetAlert(message, 'error', false, messageDetail, message === 'Invalid token' ? () => {
                    window.location.replace("/");
                } : null);
            },
        }
    );
};

export const useCheckPasswordQuery = () => {
    return useQuery('checkPassword', () => API.get('/auth/check-password'), {
        retry: (failureCount, error) => {
            // if error is 'Invalid token', don't retry
            if (error.response && error.response.data && error.response.data.msg === 'Invalid token') {
                return false;
            }
            // Default retry behavior for other errors
            return true;
        },
        onError: (error) => {
            if (error.response && error.response.data && error.response.data.msg === 'Invalid token') {
                localStorage.clear();
                window.location.replace("/auth");
            }
        }
    });
};

export const useLogoutMutation = () => {
    const queryClient = useQueryClient();
    return useMutation(
        () => {
            localStorage.clear();
            window.location.href = '/';
        },
        {
            onSuccess: () => {
                // Invalidate relevant queries or perform cleanup if necessary
                queryClient.invalidateQueries(['/auth/user/:id']);
            },
        }
    );
};
