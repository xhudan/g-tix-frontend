import { useMutation, useQuery } from 'react-query';
import SweetAlert from '../../components/alert/SweetAlert';
import API from '../api/baseApi';

export const useGetAllHistoryQuery = () => {
    return useQuery(`allBooking`, () => API.get(`/booking/all`), {
        retry: (failureCount, error) => {
            // if error is 'Invalid token', don't retry
            if (error.response && error.response.data && error.response.data.msg === 'Invalid token') {
                return false;
            }
            // Default retry behavior for other errors
            return true;
        },
        onError: (error) => {
            if (error.response && error.response.data && error.response.data.msg === 'Invalid token') {
                localStorage.clear();
                window.location.replace("/auth");
            }
        }
    });
};

export const useGetHistoryQuery = () => {
    return useQuery('booking', () => API.get('/booking'), {
        retry: (failureCount, error) => {
            // if error is 'Invalid token', don't retry
            if (error.response && error.response.data && error.response.data.msg === 'Invalid token') {
                return false;
            }
            // Default retry behavior for other errors
            return true;
        },
        onError: (error) => {
            if (error.response && error.response.data && error.response.data.msg === 'Invalid token') {
                localStorage.clear();
                window.location.replace("/auth");
            }
        }
    });
};

export const useAddBookingMutation = () => {
    return useMutation(
        (data) => API.post(`/booking/${data.movieId}?q=${data.seats.length}&cid=${data.cinema_id}&d=${data.date}&t=${data.time}`, data),
        {
            onSuccess: (response) => {
                SweetAlert("Data has been saved", "success", false, response.data.msg, () => {
                    window.location.replace("/history");
                });
            },
            onError: (error) => {
                const message =
                    (error.response && error.response.data && error.response.data.msg) ||
                    error.msg || error.toString();
                if (message === 'Invalid token') {
                    localStorage.clear();
                }
                SweetAlert(message, 'error', null, null, message === 'Invalid token' ? () => {
                    window.location.replace("/");
                } : null);
            },
        }
    );
};

export const useCancelBookingMutation = () => {
    return useMutation(
        (bookingId) => API.delete(`/booking/${bookingId}`), // Remove data parameter
        {
            onSuccess: (response) => {
                SweetAlert(response.data.msg, 'success', false, null, () => {
                    window.location.reload();
                });
            },
            onError: (error) => {
                const message =
                    (error.response && error.response.data && error.response.data.msg) ||
                    error.msg || error.toString();
                if (message === 'Invalid token') {
                    localStorage.clear();
                }
                SweetAlert(message, 'error', null, null, message === 'Invalid token' ? () => {
                    window.location.replace("/");
                } : null);
            },
        }
    );
};