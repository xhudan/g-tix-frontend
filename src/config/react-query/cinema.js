import { useMutation, useQuery } from 'react-query';
import SweetAlert from '../../components/alert/SweetAlert';
import API from '../api/baseApi';

export const useGetCinemasQuery = () => {
    return useQuery(`cinemas`, () => API.get(`/cinema`));
};

export const useGetAllCinemasQuery = () => {
    return useQuery(`allCinemas`, () => API.get(`/cinema/all`));
};

export const useAddCinemaMutation = () => {
    return useMutation(
        (data) => API.post('/cinema', data),
        {
            onSuccess: (response) => {
                SweetAlert(response.data.msg, "success", false, null, () => {
                    window.location.reload();
                });
            },
            onError: (error) => {
                const message =
                    (error.response && error.response.data && error.response.data.msg) ||
                    error.msg || error.toString();
                if (message === 'Invalid token') {
                    localStorage.clear();
                }
                SweetAlert(message, 'error', null, null, message === 'Invalid token' ? () => {
                    window.location.replace("/");
                } : null);
            },
        }
    );
};

export const useUpdateCinemaMutation = (cinemaId) => {
    return useMutation(
        (data) => API.put(`/cinema/${cinemaId}`, data),
        {
            onSuccess: (response) => {
                SweetAlert(response.data.msg, "success", false, null, () => {
                    window.location.reload();
                });
            },
            onError: (error) => {
                const message =
                    (error.response && error.response.data && error.response.data.msg) ||
                    error.msg || error.toString();
                if (message === 'Invalid token') {
                    localStorage.clear();
                }
                SweetAlert(message, 'error', null, null, message === 'Invalid token' ? () => {
                    window.location.replace("/");
                } : null);
            },
        }
    );
};

export const useDeleteCinemaMutation = () => {
    return useMutation(
        (cinemaId) => API.delete(`/cinema/${cinemaId}`),
        {
            onSuccess: (response) => {
                SweetAlert(response.data.msg, "success", false, null, () => {
                    window.location.reload();
                });
            },
            onError: (error) => {
                const message =
                    (error.response && error.response.data && error.response.data.msg) ||
                    error.msg || error.toString();
                if (message === 'Invalid token') {
                    localStorage.clear();
                }
                SweetAlert(message, 'error', null, null, message === 'Invalid token' ? () => {
                    window.location.replace("/");
                } : null);
            },
        }
    );
};
