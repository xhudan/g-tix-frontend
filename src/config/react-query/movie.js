import { useMutation, useQuery } from 'react-query';
import SweetAlert from '../../components/alert/SweetAlert';
import API from '../api/baseApi';

export const useGetAllMoviesQuery = () => {
    return useQuery(`allMovies`, () => API.get(`/movie/all`), {
        retry: (failureCount, error) => {
            // if error is 'Invalid token', don't retry
            if (error.response && error.response.data && error.response.data.msg === 'Invalid token') {
                return false;
            }
            // Default retry behavior for other errors
            return true;
        },
        onError: (error) => {
            if (error.response && error.response.data && error.response.data.msg === 'Invalid token') {
                localStorage.clear();
                window.location.replace("/auth");
            }
        }
    });
};

export const useGetMoviesQuery = () => {
    return useQuery(`movies`, () => API.get(`/movie`));
};

export const useGetRecommendationsMovieQuery = (options) => {
    return useQuery(`recommendations`, () => API.get(`/movie/recommendations`),
        {
            retry: false, // don't retry if error
            ...options, // spread the options to include enabled, etc.
            onError: (error) => {
                if (error.response && error.response.data && error.response.data.msg === 'Invalid token') {
                    localStorage.clear();
                    window.location.reload();
                }
            }
        });
};

export const useGetMovieIdQuery = (movieId, options) => {
    return useQuery(
        ['movie', movieId], 
        () => API.get(`/movie/${movieId}`),
        {
            ...options,
            onSuccess: (data) => {
                if (!data || !data.data || !data.data.data) {
                    window.location.replace("/"); // Redirect jika data null
                }
            }
        }
    );
};

export const useAddMovieMutation = () => {
    return useMutation(
        (data) => API.post('/movie', data),
        {
            onSuccess: (response) => {
                SweetAlert(response.data.msg, "success", false, null, () => {
                    window.location.reload();
                });
            },
            onError: (error) => {
                const message =
                    (error.response && error.response.data && error.response.data.msg) ||
                    error.msg || error.toString();
                if (message === 'Invalid token') {
                    localStorage.clear();
                }
                SweetAlert(message, 'error', null, null, message === 'Invalid token' ? () => {
                    window.location.replace("/");
                } : null);
            },
        }
    );
};

export const useUpdateMovieMutation = (movieId) => {
    return useMutation(
        (data) => API.put(`/movie/${movieId}`, data),
        {
            onSuccess: (response) => {
                SweetAlert(response.data.msg, "success", false, null, () => {
                    window.location.reload();
                });
            },
            onError: (error) => {
                const message =
                    (error.response && error.response.data && error.response.data.msg) ||
                    error.msg || error.toString();
                if (message === 'Invalid token') {
                    localStorage.clear();
                }
                SweetAlert(message, 'error');
            },
        }
    );
};

export const useDeleteMovieMutation = () => {
    return useMutation(
        (movieId) => API.delete(`/movie/${movieId}`),
        {
            onSuccess: (response) => {
                SweetAlert(response.data.msg, "success", false, null, () => {
                    window.location.reload();
                });
            },
            onError: (error) => {
                const message =
                    (error.response && error.response.data && error.response.data.msg) ||
                    error.msg || error.toString();
                SweetAlert(message, 'error');
            },
        }
    );
};

export const useSyncMLQuery = (options) => {
    return useQuery('trainModel', () => API.get('/movie/trainModel'), options);
};
