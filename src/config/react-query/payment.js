import { useMutation } from 'react-query';
import SweetAlert from '../../components/alert/SweetAlert';
import API from '../api/baseApi';

export const useMakePaymentMutation = () => {
    return useMutation(
        async (bookingCode) => {
            const response = await API.post(`/payment/${bookingCode}`);
            return response.data; // return data from API response
        },
        {
            onSuccess: (data) => {
                window.open(data.redirectUrl, '_blank');
            },
            onError: (error) => {
                const message =
                    (error.response && error.response.data && error.response.data.msg) ||
                    error.msg || error.toString();
                const messageDetail =
                    (error.response && error.response.data && error.response.data.msgDetail) ||
                    error.msgDetail || null;
                if (message === 'Invalid token') {
                    localStorage.clear();
                }
                SweetAlert(message, 'error', false, messageDetail, message === 'Invalid token' ? () => {
                    window.location.replace("/");
                } : null);
            },
        }
    );
};
