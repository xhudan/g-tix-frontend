import { useQuery } from 'react-query';
import API from '../api/baseApi';
import SweetAlert from '../../components/alert/SweetAlert';

export const useSeatCheckerQuery = (movieId, cid, d, t) => {
    return useQuery(`checkseat`, () => API.get(`/checkseat/${movieId}?cid=${cid}&d=${d}&t=${t}`), {
        retry: false, // dont retry if error
        onError: (error) => {
            const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
            if (message === 'Invalid token') {
                localStorage.clear();
            }
            SweetAlert(message, 'error', false, null, () => {
                message === 'Invalid token' ? window.location.replace("/") : window.location.replace("/auth");
            });
        },
    });
};