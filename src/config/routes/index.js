import React, { useEffect } from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import { useAuth } from "../../components/context/authContext";
import SweetAlertFetchingData from "../../components/alert/SweetAlertFetchingData";
import Auth from "../../pages/Auth";
import Home from "../../pages/Home";
import Movie from "../../pages/Movie";
import Seat from "../../pages/Seat";
import Profile from "../../pages/Profile";
import History from "../../pages/History";
import Admin from "../../pages/Admin";
import CreateUpdateMovie from "../../pages/CreateUpdateMovie";
import Verify from "../../pages/Verify";
import NotFoundPage from "../../pages/NotFoundPage";

const AppRouter = () => {
  const { token, userRole, isLoading } = useAuth();

  useEffect(() => {
      if (isLoading) {
          SweetAlertFetchingData("Loading...");
      } else {
          SweetAlertFetchingData.close();
      }
  }, [isLoading]);

  if (isLoading) {
    return <div className="h-dvh bg-neutral-900 w-full"></div>;
  }

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/movie/:movieId" element={<Movie />} />
        <Route path="/auth" element={token ? <Navigate to="/" /> : <Auth />} />
        <Route path="/verify" element={token ? <Navigate to="/" /> : <Verify />} />
        <Route path="/check/:movieId" element={token ? <Seat /> : <Navigate to="/auth" />} />
        <Route path="/profile" element={token ? <Profile /> : <Navigate to="/auth" />} />
        <Route path="/history" element={token ? <History /> : <Navigate to="/auth" />} />
        <Route path="/manage" element={userRole === "Admin" ? <Admin /> : !token ? <Navigate to="/auth" /> : <Navigate to="/" />} />
        <Route path="/manage/movie" element={userRole === "Admin" ? <CreateUpdateMovie /> : !token ? <Navigate to="/auth" /> : <Navigate to="/" />} />
        <Route path="/manage/movie/:movieId" element={userRole === "Admin" ? <CreateUpdateMovie /> : !token ? <Navigate to="/auth" /> : <Navigate to="/" />} />
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
    </BrowserRouter>
  );
};

export default AppRouter;
