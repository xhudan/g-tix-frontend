import React from 'react';
import Navbar from '../components/navbar/Navbar';
import Menu from '../components/admin/Menu';
import Users from '../components/admin/Users';
import Bookings from '../components/admin/Bookings';
import Movies from '../components/admin/Movies';
import Statistics from '../components/admin/Statistics';
import Cinemas from '../components/admin/Cinemas';
import { useLocation } from 'react-router-dom';

const Admin = () => {
    const location = useLocation();
    const hash = location.hash;

    return (
        <div className="h-dvh bg-neutral-900 w-full overflow-auto modern-scrollbar">
            <Navbar />
            <div className="py-6 mx-5 sm:mx-10 md:mx-20 space-y-8">
                <Menu />
                {hash === '#users' && <Users />}
                {hash === '#cinemas' && <Cinemas />}
                {hash === '#movies' && <Movies />}
                {hash === '#bookings' && <Bookings />}
                {hash === '#statistics' && <Statistics />}
            </div>
        </div>
    );
};

export default Admin;

// <div className="py-8 mx-5 sm:mx-10 md:mx-20">
//    <div className="flex flex-col lg:flex-row gap-8">
//        <div className="w-full lg:w-max">
//            <Menu />
//        </div>
//        <div className="flex-grow w-full overflow-hidden">
//            {hash === '#users' && <Users />}
//            {hash === '#cinemas' && <Cinemas />}
//            {hash === '#movies' && <Movies />}
//            {hash === '#bookings' && <Bookings />}
//            {hash === '#statistics' && <Statistics />}
//        </div>
//    </div>
//</div>
