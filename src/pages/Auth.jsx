import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import gtixLogo from '../assets/G-Tix_600.png';
import SignIn from '../components/auth/SignIn';
import SignUp from '../components/auth/SignUp';
import ResetPassword from '../components/auth/ResetPassword';
import { useLoginMutation, useRegisterMutation, useResetPasswordMutation, useOAuthMutation } from '../config/react-query/auth';
import { GoogleLogin } from '@react-oauth/google';
// import { FaGoogle } from "react-icons/fa";

const Auth = () => {
  const [form, setForm] = useState('signIn'); // 'signIn', 'signUp', or 'resetPassword'

  const toggleForm = (formName) => {
    setForm(formName);
  };

  const oAuthMutation = useOAuthMutation();
  const loginMutation = useLoginMutation();
  const registerMutation = useRegisterMutation();
  const resetPasswordMutation = useResetPasswordMutation();

  const handleOAuth = (credentialResponse) => {
    const token = credentialResponse.credential;
    oAuthMutation.mutateAsync({ token });
  };

  const handleLogin = (data) => {
    loginMutation.mutateAsync(data);
  };

  const handleRegister = (data) => {
    registerMutation.mutateAsync(data);
  };

  const handleResetPassword = (data) => {
    resetPasswordMutation.mutateAsync(data);
  };

  return (
    <section className="bg-gradient-to-tr from-pink-900 to-sky-900 h-screen">
      <div className="transform scale-75 2xl:transform 2xl:scale-100 flex flex-col h-full items-center justify-center mx-5 sm:mx-10 md:mx-20">
        <div className="w-full rounded-lg shadow-inner shadow-white border max-w-md bg-neutral-900 border-gray-700">
          <Link to="/" draggable={false}>
            <img className="mx-auto my-6 px-8" src={gtixLogo} alt="G-Tix" draggable={false} />
          </Link>
          {form === 'signIn' && (
            <>
              <p className="text-sm text-gray-400 text-center">
                Don’t have an account yet?{' '}
                <button onClick={() => toggleForm('signUp')} className="font-medium hover:underline text-blue-500">
                  Sign up
                </button>
              </p>
              <SignIn onSubmit={handleLogin} toggleForm={toggleForm} />
            </>
          )}
          {form === 'signUp' && (
            <>
              <p className="text-sm text-gray-400 text-center">
                Already have an account?{' '}
                <button onClick={() => toggleForm('signIn')} className="font-medium hover:underline text-blue-500">
                  Sign in
                </button>
              </p>
              <SignUp onSubmit={handleRegister} />
            </>
          )}
          {form === 'resetPassword' && (
            <>
              <p className="text-sm text-gray-400 text-center">
                Remember your password?{' '}
                <button onClick={() => toggleForm('signIn')} className="font-medium hover:underline text-blue-500">
                  Sign in
                </button>
              </p>
              <ResetPassword onSubmit={handleResetPassword} />
            </>
          )}
          <div className="px-8 pb-8 text-white">
            <div className="flex items-center space-x-2 my-4">
              <hr className="flex-grow border-gray-400" />
              <span className="px-2 text-gray-400">Or</span>
              <hr className="flex-grow border-gray-400" />
            </div>
            <div className="flex items-center mx-auto justify-center space-x-2">
              {/* <p>{form === "signUp" ? "Sign up" : "Sign in"} with</p>
              <button onClick={() => { }} className="bg-blue-500 hover:bg-blue-600 p-2 rounded-full">
                <FaGoogle />
              </button> */}
              <GoogleLogin
                onSuccess={handleOAuth}
                useOneTap
              />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Auth;