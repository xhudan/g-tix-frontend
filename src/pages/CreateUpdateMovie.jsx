import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useGetMovieIdQuery, useAddMovieMutation, useUpdateMovieMutation } from "../config/react-query/movie";
import SweetAlertFetchingData from "../components/alert/SweetAlertFetchingData";
import Navbar from "../components/navbar/Navbar";
import TagsInput from "../components/create_update_movie/TagsInput";

const CreateUpdateMovie = () => {
    const { movieId } = useParams();
    const [movieData, setMovieData] = useState({});
    const [selectedFile, setSelectedFile] = useState(null);
    const [hasLoad, setHasLoad] = useState(false);

    const addMovieMutation = useAddMovieMutation();
    const updateMovieMutation = useUpdateMovieMutation(movieId);

    const { data: movies, isLoading: moviesLoading } = useGetMovieIdQuery(movieId, {
        enabled: !!movieId && !hasLoad, // Only fetch when movieId is present and not yet fetched
    });

    useEffect(() => {
        if (movieId && moviesLoading) {
            SweetAlertFetchingData("Loading...");
        } else {
            SweetAlertFetchingData.close();
        }

        if (movieId && !moviesLoading) {
            setMovieData(movies?.data?.data);
            setHasLoad(true);
        }
    }, [moviesLoading, movieId, movies]);

    if (moviesLoading || (!movieData && movieId)) {
        return <div className="h-dvh bg-neutral-900 w-full"></div>;
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const formData = new FormData();

        if (selectedFile) {
            formData.append("poster", selectedFile);
        }

        // Append each key-value pair from movieData to formData
        for (const key in movieData) {
            if (Array.isArray(movieData[key])) {
                if (movieData[key].length === 0) {
                    // Append the key with empty string if the array is empty
                    formData.append(`${key}[]`, "");
                } else {
                    // If it's an array, loop through its elements and append each one
                    movieData[key].forEach((value, index) => {
                        formData.append(`${key}[${index}]`, value);
                    });
                }
            } else {
                formData.append(key, movieData[key]); // null value will be represented as string "null", adjust accordingly in the backend controller 
            }
        }

        if (movieId) {
            updateMovieMutation.mutate(formData);
        } else {
            addMovieMutation.mutate(formData);
        }
    };

    const handleChange = (field, value, type) => {
        const formattedValue = type === "time" ? formatPartialTime(value.replace(/\D/g, '')) :
            type === "number" ? value.replace(/\D/g, '') : value;
        setMovieData((prevData) => ({
            ...prevData,
            [field]: formattedValue,
        }));
    };

    const handleFileChange = (e) => {
        setSelectedFile(e.target.files[0]); // Update selected file
    };

    const formatPartialTime = (time) => {
        const onlyNumbers = time.slice(0, 4); // Limit input to 4 digits
        const hours = onlyNumbers.slice(0, 2);
        const minutes = onlyNumbers.slice(2, 4);
        let formattedTime = hours;

        if (minutes.length > 0) {
            formattedTime += `:${minutes}`;
        }

        // Validate the hours and minutes
        if (hours && (parseInt(hours) > 23 || parseInt(hours) < 0)) {
            return ""; // Invalid hours
        }
        if (minutes && (parseInt(minutes) > 59 || parseInt(minutes) < 0)) {
            return ""; // Invalid minutes
        }

        return formattedTime;
    };

    const formattedTimeOptions = (time) => {
        if (!time) return ""; // Check for undefined or null values
        const [hours, minutes] = time?.split(':');
        return `${hours}${minutes ? `:${minutes}` : ""}`;
    };

    const inputType = [
        { label: "Title", type: "text", htmlFor: "title", id: "title", value: movieData?.title, mandatory: true },
        { label: "Language", type: "text", htmlFor: "language", id: "language", value: movieData?.language },
        { label: "Duration (HH:MM format)", type: "time", htmlFor: "duration", id: "duration", value: formattedTimeOptions(movieData?.duration) },
        { label: "Age", type: "number", htmlFor: "age", id: "age", value: movieData?.age },
        { label: "Start date", type: "date", htmlFor: "start_date", id: "start_date", value: movieData?.start_date, mandatory: true },
        { label: "Runtime", type: "date", htmlFor: "expired", id: "expired", value: movieData?.expired, mandatory: movieId },
        { label: "Price", type: "number", htmlFor: "price", id: "price", value: movieData.price && `Rp${movieData.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}` },
    ];

    const inputTagsType = [
        { key: "director", label: "Director", type: "text" },
        { key: "writer", label: "Writer", type: "text" },
        { key: "actors", label: "Actors", type: "text" },
        { key: "genre", label: "Genre", type: "text", mandatory: true },
        { key: "showtime_list", label: "Showtime list", type: "time" },
    ];

    return (
        <section className="h-dvh bg-neutral-900 w-full overflow-auto modern-scrollbar place-items-center">
            <Navbar />
            <div className="py-6 mx-5 sm:mx-10 md:mx-20 text-white">
                <h1 className="text-2xl rounded-2xl bg-gray-500 font-semibold mb-6 text-center">{movieId ? "Edit Movie" : "Add Movie"}</h1>
                <div className="flex flex-col justify-center gap-6 lg:flex-row">
                    {movieData?.poster &&
                        <div className="max-w-[300px] mx-auto items-center flex">
                            <img src={movieData.poster} alt={movieData.title} />
                        </div>
                    }
                    <form onSubmit={handleSubmit} className="w-full items-center" encType="multipart/form-data">
                        <div className="grid grid-cols-1 sm:grid-cols-2 gap-4 w-full">
                            {inputType.map((item, index) => (
                                <div key={index} className="relative mb-2">
                                    <input
                                        type={item.type === "time" || item.type === "number" ? "text" : item.type}
                                        id={item.id}
                                        className="block px-3 py-3 w-full bg-transparent rounded-lg border border-gray-400 peer focus:border-red-500 focus:outline-none"
                                        placeholder=""
                                        value={item.value || ""}
                                        onChange={(e) => handleChange(item.id, item.type === "file" ? e.target.files[0] : e.target.value, item.type)}
                                        accept={item.type === "file" ? "image/png, image/jpeg" : undefined}
                                    />
                                    <label
                                        htmlFor={item.htmlFor}
                                        className="absolute rounded-sm text-gray-400 bg-neutral-900 duration-300 transform -translate-y-5 scale-75 top-2 z-10 origin-[0] px-2 peer-focus:px-2 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-5 left-1"
                                    >
                                        {item.label}{item.mandatory && <span className="text-red-500">*</span>}
                                    </label>
                                </div>
                            ))}
                            <div className="relative mb-2">
                                <input
                                    type="file"
                                    id="poster"
                                    className="block px-3 py-3 w-full bg-transparent rounded-lg border border-gray-400 peer focus:border-red-500 focus:outline-none"
                                    onChange={handleFileChange}
                                    accept="image/png, image/jpeg"
                                />
                                <label
                                    htmlFor="poster"
                                    className="absolute rounded-sm text-gray-400 bg-neutral-900 duration-300 transform -translate-y-5 scale-75 top-2 z-10 origin-[0] px-2 peer-focus:px-2 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-5 left-1"
                                >
                                    Poster
                                </label>
                            </div>
                            {inputTagsType.map(({ key, label, type, mandatory }) => (
                                <TagsInput
                                    key={key}
                                    type={type}
                                    label={label}
                                    id={key}
                                    values={key === "showtime_list" ? movieData?.[key]?.map(formattedTimeOptions) : movieData?.[key] || []}
                                    onChange={(values) => handleChange(key, values)}
                                    mandatory={mandatory}
                                />
                            ))}
                        </div>

                        <div className="relative my-4">
                            <textarea
                                type="text"
                                id="synopsis"
                                className="block px-3 pb-3 pt-4 w-full bg-transparent rounded-lg border border-gray-400 peer focus:border-red-500 focus:outline-none"
                                placeholder=""
                                value={movieData?.synopsis || ""}
                                onChange={(e) => handleChange("synopsis", e.target.value)}
                            />
                            <label
                                htmlFor="synopsis"
                                className="absolute rounded-sm text-gray-400 bg-neutral-900 duration-300 transform -translate-y-5 scale-75 top-2 z-10 origin-[0] px-2 peer-focus:px-2 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-5 left-1"
                            >
                                Synopsis
                            </label>
                        </div>

                        <div className="text-center space-y-4">
                            <button type="submit" className="w-full py-1.5 bg-blue-500 hover:bg-blue-600 rounded-2xl">
                                Submit
                            </button>
                            <button type="button" onClick={() => window.history.back()} className="w-full py-1.5 bg-red-500 hover:bg-red-600 rounded-2xl">
                                Back
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    );
};

export default CreateUpdateMovie;
