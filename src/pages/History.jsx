import React, { useState } from "react";
import Navbar from "../components/navbar/Navbar";
import { useGetHistoryQuery, useCancelBookingMutation } from "../config/react-query/book";
import { useMakePaymentMutation } from "../config/react-query/payment";
import HistoryCard from "../components/history/HistoryCard";
import SweetAlert from "../components/alert/SweetAlert";

const History = () => {
  const { data } = useGetHistoryQuery();
  const historyData = data?.data.data || [];

  const sortedHistories = [...historyData].sort((a, b) =>
    (b.createdAt || "").localeCompare(a.createdAt || "")
  );

  const cancelBookingMutation = useCancelBookingMutation();
  const makePaymentMutation = useMakePaymentMutation();

  const [currentPage, setCurrentPage] = useState(0);

  const cancelBooking = (bookingId) => {
    SweetAlert("Are you sure?", "warning", true, null, () => {
      cancelBookingMutation.mutate(bookingId);
    });
  };

  const makePayment = (bookingCode) => {
    makePaymentMutation.mutate(bookingCode);
  };

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const itemsPerPage = 5;
  const startIndex = currentPage * itemsPerPage;
  const endIndex = Math.min(startIndex + itemsPerPage, sortedHistories.length);
  const currentItems = sortedHistories.slice(startIndex, endIndex);

  return (
    <div className="h-dvh bg-neutral-900 w-full overflow-auto modern-scrollbar">
      <Navbar />
      <div className="py-6 mx-5 sm:mx-10 md:mx-20">
        {sortedHistories.length > 0 ? (
          <>
            <HistoryCard
              items={currentItems}
              cancelBooking={cancelBooking}
              makePayment={makePayment}
            />
            {sortedHistories.length > currentItems.length &&
              <div className="flex flex-wrap mx-auto justify-center mt-4">
                {Array.from({ length: Math.ceil(sortedHistories.length / itemsPerPage) }, (_, i) => (
                  <button
                    key={i}
                    className={`min-w-10 py-1 m-1 rounded-md ${currentPage === i ? "bg-blue-500 text-white" : "bg-gray-300 text-gray-800"
                      }`}
                    onClick={() => handlePageChange(i)}
                  >
                    {i + 1}
                  </button>
                ))}
              </div>
            }
          </>
        ) : (
          <div className="text-center text-gray-300 text-white">
            There is no booking history yet
          </div>
        )}
      </div>
    </div>
  );
};

export default History;
