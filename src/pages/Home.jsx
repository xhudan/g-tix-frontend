import React, { useEffect } from "react";
import Navbar from "../components/navbar/Navbar";
import Movies from "../components/movie/Movies";
import { useGetMoviesQuery, useGetRecommendationsMovieQuery } from "../config/react-query/movie";
import { useAuth } from "../components/context/authContext";
import SweetAlertFetchingData from '../components/alert/SweetAlertFetchingData';

const Home = () => {
  const { token } = useAuth();

  const { data: movies, isLoading: moviesLoading } = useGetMoviesQuery();
  const { data: recommendations, isLoading: recommendationsLoading } = useGetRecommendationsMovieQuery({ enabled: !!token, token});

  const moviesData = movies?.data.data || [];
  const recommendationsData = recommendations?.data.recommendedMovies || [];

  useEffect(() => {
    if (moviesLoading || recommendationsLoading) {
      SweetAlertFetchingData("Loading...");
    } else {
      SweetAlertFetchingData.close();
    }
  }, [moviesLoading, recommendationsLoading]);

  if (moviesLoading || recommendationsLoading) {
    return <div className="h-dvh bg-neutral-900 w-full"></div>;
  }

  const now = new Date().toLocaleDateString('en-CA');

  const nowPlayingMovies = moviesData.filter(
    (movie) => movie.start_date <= now && now <= movie.expired
  );

  const upcomingMovies = moviesData.filter(
    (movie) => movie.start_date > now
  );

  return (
    <div className="h-dvh bg-neutral-900 w-full overflow-auto modern-scrollbar">
      <Navbar />
      <div className="py-6 mx-5 sm:mx-10 md:mx-20">
        {recommendationsData.length > 0 && (
          <>
            <h1 className="bg-red-500 text-center text-2xl font-bold text-white">RECOMMENDATIONS</h1>
            <Movies items={recommendationsData} />
          </>
        )}
        <h1 className="bg-red-500 text-center text-2xl font-bold text-white">NOW PLAYING</h1>
        <Movies items={nowPlayingMovies} />
        <h1 className="bg-red-500 text-center text-2xl font-bold text-white">UPCOMING</h1>
        <Movies items={upcomingMovies} />
      </div>
    </div>
  );
};

export default Home;
