import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useGetMovieIdQuery } from '../config/react-query/movie';
import { useGetCinemasQuery } from '../config/react-query/cinema';
import Navbar from '../components/navbar/Navbar';
import MovieId from '../components/movie/MovieId';
import Synopsis from '../components/movie/Synopsis';
import SweetAlertFetchingData from '../components/alert/SweetAlertFetchingData';

const Movie = () => {
    const { movieId } = useParams();
    const { data: movies, isLoading: moviesLoading } = useGetMovieIdQuery(movieId);
    const { data: cinemas, isLoading: cinemasLoading } = useGetCinemasQuery();

    useEffect(() => {
        if (moviesLoading || cinemasLoading) {
            SweetAlertFetchingData("Loading...");
        } else {
            SweetAlertFetchingData.close();
        }
    }, [moviesLoading, cinemasLoading]);

    if (moviesLoading || cinemasLoading) {
        return <div className="h-dvh bg-neutral-900 w-full"></div>;
    }

    const movieData = movies?.data.data;
    const cinemasData = cinemas?.data.data || [];

    const now = new Date();
    const startDate = new Date(movieData.start_date);
    const expiredDate = new Date(movieData.expired);

    let isPlaying;

    if (startDate <= now && now <= expiredDate) {
        isPlaying = "NOW PLAYING";
    } else if (startDate > now) {
        isPlaying = "UPCOMING";
    } else {
        isPlaying = "NOT AVAILABLE";
    }

    return (
        <div className="h-dvh bg-neutral-900 w-full overflow-auto modern-scrollbar">
            <Navbar />
            <div className="py-6 mx-5 sm:mx-10 md:mx-20">
                <h1 className="bg-red-500 text-center text-2xl font-bold text-white">
                    {isPlaying}
                </h1>
                <div className="flex flex-col my-6 lg:flex-row lg:px-6 gap-8">
                    <div className="mx-auto">
                        <MovieId movie={movieData} />
                    </div>
                    <div className="flex-grow w-full overflow-hidden">
                        <Synopsis isPlaying={isPlaying} cinema={cinemasData} movie={movieData} />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Movie;