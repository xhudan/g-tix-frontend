const NotFoundPage = () => {
  return (
    <div className="flex justify-center text-center items-center h-dvh bg-gradient-to-tr from-pink-900 to-sky-900">
      <div className="mx-5 sm:mx-10 md:mx-20 bg-neutral-900 shadow overflow-hidden shadow-inner shadow-white border rounded-lg p-6">
        <h1 className="text-9xl font-bold text-red-600">404</h1>
        <h1 className="text-3xl font-bold text-white py-8">Page not found</h1>
        <p className="text-white">The page you are trying to access does not exist.</p>
        <button onClick={(e) => { window.location.replace("/") }}
          className="mt-6 bg-blue-600 hover:bg-blue-700 text-white font-semibold px-6 py-2 rounded-xl"
        >
          Home
        </button>
      </div>
    </div>
  );
}

export default NotFoundPage;
