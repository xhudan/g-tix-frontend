import React, { useEffect } from 'react';
import Navbar from '../components/navbar/Navbar';
import Menu from '../components/profile/Menu';
import Name from '../components/profile/Name';
import Password from '../components/profile/Password';
import { useLocation } from 'react-router-dom';
import { useCheckPasswordQuery } from '../config/react-query/auth';

const Profile = () => {
    const location = useLocation();
    const hash = location.hash;

    useEffect(() => {
        if (!hash) {
            window.location.replace('/profile#user');
        }
    }, [hash]);

    const { data: checkPassword, isLoading: checkPasswordLoading } = useCheckPasswordQuery();

    const isPassword = checkPassword?.data.isPassword;
    
    if (checkPasswordLoading) {
      return <div className="h-dvh bg-neutral-900 w-full"></div>;
    }

    return (
        <div className="h-dvh bg-neutral-900 w-full overflow-auto modern-scrollbar">
            <Navbar />
            <div className="py-6 mx-5 sm:mx-10 md:mx-20">
                <div className="flex flex-col lg:flex-row gap-8">
                    <div className="mx-auto w-full lg:w-max">
                        <Menu isPassword={isPassword} />
                    </div>
                    <div className="flex-grow w-full overflow-hidden">
                        {hash === '#user' && <Name />}
                        {hash === '#security' && <Password isPassword={isPassword} />}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Profile;
