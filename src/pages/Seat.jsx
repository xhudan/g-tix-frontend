import React, { useEffect } from 'react';
import { useParams, useLocation } from 'react-router-dom';
import { useGetMovieIdQuery } from '../config/react-query/movie';
import { useSeatCheckerQuery } from '../config/react-query/seatChecker';
import Navbar from '../components/navbar/Navbar';
import SeatPicker from '../components/seat/SeatPicker';
import SweetAlertFetchingData from '../components/alert/SweetAlertFetchingData';

const Seat = () => {
    const { movieId } = useParams();
    const query = new URLSearchParams(useLocation().search);

    const cid = query.get("cid");
    const d = query.get("d");
    const t = query.get("t");

    const { data: movies, isLoading: moviesLoading } = useGetMovieIdQuery(movieId);
    const { data: seats, isLoading: seatsLoading, isError } = useSeatCheckerQuery(movieId, cid, d, t);

    useEffect(() => {
        if (moviesLoading || seatsLoading) {
            SweetAlertFetchingData("Loading...");
        } else {
            SweetAlertFetchingData.close();
        }
    }, [moviesLoading, seatsLoading]);

    const movieData = movies?.data.data;
    const seatChecker = seats?.data.data;

    if (moviesLoading || seatsLoading || movieData === null || isError) {
        return <div className="h-dvh bg-neutral-900 w-full"></div>;
    }
    
    const moviePrice = movieData.price;

    return (
        <div className="min-h-screen bg-neutral-900">
            <Navbar />
            <div className="py-6 mx-5 sm:mx-10 md:mx-20">
                <h1 className="bg-red-500 text-center text-2xl font-bold text-white">SCREEN</h1>
                <SeatPicker seatChecker={seatChecker} moviePrice={moviePrice} movieId={movieId} cid={cid} d={d} t={t}/>
            </div>
        </div>
    );
};

export default Seat;