import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import gtixLogo from '../assets/G-Tix_600.png';
import { useLocation } from 'react-router-dom';
import { useVerifyOTPMutation } from '../config/react-query/auth';
import { FaEye, FaEyeSlash } from 'react-icons/fa';
import SweetAlert from '../components/alert/SweetAlert';

const Verify = () => {
    const query = new URLSearchParams(useLocation().search);
    const [showPassword, setShowPassword] = useState(false);
    const [showConfirmPassword, setShowConfirmPassword] = useState(false);
    const [hasLoaded, setHasLoaded] = useState(false);
    const [result, setResult] = useState(null);

    const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
    };

    const toggleConfirmPasswordVisibility = () => {
        setShowConfirmPassword(!showConfirmPassword);
    };

    const account = query.get("account");
    const reset = query.get("reset");

    let type;
    if (reset && !account) {
        type = "reset";
    } else if (!reset && account) {
        type = "account";
    } else {
        window.location.replace("/");
    }

    const verifyOTPMutation = useVerifyOTPMutation(type, account || reset);

    useEffect(() => {
        if (!hasLoaded) {
            verifyOTPMutation.mutate(
                {},
                {
                    onSuccess: (response) => {
                        setResult(response.data.msg);
                        setHasLoaded(true);
                    },
                    onError: (error) => {
                        const message =
                            (error.response && error.response.data && error.response.data.msg) ||
                            error.msg || error.toString();
                        setResult(message);
                        setHasLoaded(true);
                    }
                }
            );
        }

        return () => {
            // Cleanup function to prevent re-running the effect
            setHasLoaded(true);
        };
    }, [hasLoaded, verifyOTPMutation]);

    // Validate 8 character numbers and letters
    const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;

    const handlePasswordSubmit = async (event) => {
        event.preventDefault();

        const formData = new FormData(event.target);
        const data = Object.fromEntries(formData.entries());

        if (data.password !== data.confirm_password) {
            return SweetAlert("Password and confirm password don't match!", "error");
        } else if (!passwordRegex.test(data.password)) {
            return SweetAlert("The new password is less secure!", "error", null, "New password must contain at least 8 characters with at least one letter and one number");
        } else {
            await verifyOTPMutation.mutate(
                { password: data.password },
                {
                    onSuccess: (response) => {
                        setResult(response.data.msg);
                        setHasLoaded(true);
                    },
                    onError: (error) => {
                        const message =
                            (error.response && error.response.data && error.response.data.msg) ||
                            error.msg || error.toString();
                        setResult(message);
                        setHasLoaded(true);
                    }
                });
        }
    };

    const resultArray = result?.split('.');

    const filteredResultArray = resultArray?.filter(sentence => sentence.trim());

    const resultElements = filteredResultArray?.map((sentence, index) => (
      <p key={index}>
        {sentence.trim()}.
      </p>
    ));
    
    return (
        <section className="bg-gradient-to-tr from-pink-900 to-sky-900 h-screen">
            <div className="flex h-full items-center justify-center mx-5 sm:mx-10 md:mx-20">
                <div className="w-full rounded-lg shadow-inner shadow-white border max-w-md bg-neutral-900 border-gray-700">
                    <Link to="/" draggable={false}>
                        <img className="mx-auto mt-6 px-8" src={gtixLogo} alt="G-Tix" draggable={false} />
                    </Link>
                    {reset && result === "Token is valid" ? (
                        <div className="p-8 space-y-4">
                            <h1 className="text-2xl font-bold text-white">
                                Reset password
                            </h1>
                            <form className="space-y-4" onSubmit={handlePasswordSubmit}>
                                <div>
                                    <label htmlFor="password" className="block mb-2 text-sm font-medium text-white">
                                        Password
                                    </label>
                                    <span className='flex items-center relative'>
                                        <input
                                            type={showPassword ? 'text' : 'password'}
                                            name="password"
                                            placeholder="••••••••"
                                            className="border rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white focus:ring-blue-500 focus:border-blue-500"
                                            required={true}
                                        />
                                        <button
                                            type="button"
                                            className="absolute inset-y-0 right-0 pr-3"
                                            onClick={togglePasswordVisibility}
                                        >
                                            {showPassword ? <FaEyeSlash className="text-white" /> : <FaEye className="text-white" />}
                                        </button>
                                    </span>
                                </div>
                                <div>
                                    <label htmlFor="confirm_password" className="block mb-2 text-sm font-medium text-white">
                                        Confirm password
                                    </label>
                                    <span className='flex items-center relative'>
                                        <input
                                            type={showConfirmPassword ? 'text' : 'password'}
                                            name="confirm_password"
                                            placeholder="••••••••"
                                            className="border rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white focus:ring-blue-500 focus:border-blue-500"
                                            required={true}
                                        />
                                        <button
                                            type="button"
                                            className="absolute inset-y-0 right-0 pr-3"
                                            onClick={toggleConfirmPasswordVisibility}
                                        >
                                            {showConfirmPassword ? <FaEyeSlash className="text-white" /> : <FaEye className="text-white" />}
                                        </button>
                                    </span>
                                </div>
                                <button
                                    type="submit"
                                    className="w-full text-white focus:ring-4 focus:outline-none font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-blue-600 hover:bg-blue-700 focus:ring-blue-800"
                                >
                                    Submit
                                </button>
                            </form>
                        </div>
                    ) : (
                        <div className="my-40 px-8 text-white text-center">
                            {resultElements}
                        </div>
                    )}
                </div>
            </div>
        </section>
    );
};

export default Verify;
